
class showMoreElements
{
    constructor(elementClass, initialLimit)
    {
        this.elementClass   = elementClass;
        this.limit          = initialLimit;
    }

    viewMore(num)
    {
        this.limit += num;
        let classObject = this;
        $('.'+this.elementClass).each(function (index) {
            let self = $(this);
            if(index < classObject.limit) self.show();
        });
    }
}