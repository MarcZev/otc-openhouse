@extends('auth.layouts.master')
@section('content')
<body id="sign-in" class="main-layout">
    <div class="container-fluid body">
        <!-- LOGO -->
        <a href="#" >
            <img src="{{asset('images/logo.png')}}" class="mt-2 ml-3">
        </a>
        <!-- Content -->
        <div class="container content mt-5">
            <div class="row">
                <div class="col-sm-6" id="reg">
                    <center>
                        <p>Don't have an account?</p>
                        <a href="{{route('register')}}"><button class="btn btn-outline-danger">REGISTER NOW</button></a>
                    </center>
                </div>

                <div class="col-sm-6" id="sign">
                    <form action="{{route('login')}}" method="POST">
                        <h4 class="text-center mb-5">SIGN IN</h4>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div>
                            <label class="mb-0">Email Address</label>
                            <input type="email" name="email" class="form-control mb-2" required>
                            @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div>
                            <label class="mb-0">Password</label>
                            <input type="password" name="password" class="form-control" required>
                            @if($errors->has('password'))
                                <span class="help-block">{{$errors->first('password')}}</span>
                            @endif
                        </div>
                        <a href="{{ route('password.request') }}" class="float-right mb-3 mt-2">Forgot Password</a>
                        <button type="submit" class="btn btn-danger form-control mt-4">SIGN IN</button>
                    </form>
                </div>
            </div>
        </div>
        <footer>
            <p class="pt-2">&copy; OTC Open House 2019</p>
        </footer>
    </div>
</body>
@endsection