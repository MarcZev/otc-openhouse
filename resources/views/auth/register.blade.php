@extends('auth.layouts.master')
@section('content')
<body id="register">
    <div class="container-fluid body" style="height: 100% !important;">
        <!-- LOGO -->
        <a href="#" >
            <img src="{{asset('images/logo.png')}}" class="mt-2 ml-3">
        </a>
        <!-- Content -->
        <div class="container content mt-3 mb-4">
            <div class="row">
                <div class="col-sm-6" id="sign">
                    <center>
                        <p>Already have an account?</p>
                        <a href="{{route('login')}}"><button class="btn btn-outline-danger">SIGN IN</button></a>
                    </center>
                </div>
                <div class="col-sm-6 py-4" id="reg">
                    <form action="{{route('register')}}" method="POST">
                        <h4 class="text-center">REGISTER</h4>
                        <input type="hidden" value="{{csrf_token()}}" name="_token">
                        <input type="hidden" value="" name="request_id">
                        <div class="form-group row mb-1 mt-0" style="border:none;">
                            <div class="col-sm-6">
                                <label for="first_name" class=" col-form-label mb-0">First Name <span class="help-block">*</span></label>
                                <input name="first_name" value="{{old('first_name') ?? NULL}}" type="text" class="form-control mt-0 req-f" id="fname" required>
                                <span class="help-block hidden req">First name is required.</span>
                                @if($errors->has('first_name'))
                                    <span class="help-block">{{$errors->first('first_name')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label for="last_name" class="col-form-label  mb-0">Last Name <span class="help-block">*</span></label>
                                <input name="last_name" value="{{old('last_name') ?? NULL}}" type="text" class="form-control mt-0 req-f" id="lname" required>
                                <span class="help-block hidden req">Last name is required.</span>
                                @if($errors->has('last_name'))
                                    <span class="help-block">{{$errors->first('last_name')}}</span>
                                @endif
                            </div>
                        </div>

                        <div>
                            <label for="phone" class="mb-0">Phone Number <span class="help-block">*</span></label>
                            <input maxlength="14" value="{{old('phone') ?? NULL}}" type="text" name="phone" class="form-control mb-1 req-f" required>
                            <span class="help-block hidden" id="phone_errors"></span>
                            <span class="help-block hidden req">Phone number is required.</span>
                            @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                            @endif
                        </div>

                        <div>
                            <label for="company_name" class="mb-0">Company Name <span class="help-block">*</span></label>
                            <input type="text" value="{{old('company_name') ?? NULL}}" name="company_name" class="form-control mb-1 req-f" required>
                            <span class="help-block hidden req">Company name is required.</span>
                            @if($errors->has('company_name'))
                                <span class="help-block">{{$errors->first('company_name')}}</span>
                            @endif
                        </div>

                        <div>
                            <label for="email" class="mb-0">Email Address <span class="help-block">*</span></label>
                            <input type="email" value="{{old('email') ?? NULL}}" name="email" class="form-control mb-1 req-f" required>
                            <span class="help-block hidden req">Email is required.</span>
                            @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div>
                            <label for="password" class="mb-0">Password <span class="help-block">*</span></label>
                            <input type="password" name="password" class="form-control mb-1 req-f" required>
                            <span class="help-block hidden req">Password is required.</span>
                            @if($errors->has('password'))
                                <span class="help-block">{{$errors->first('password')}}</span>
                            @endif
                        </div>
                        <div>
                            <label for="password-confirm" class="mb-0">Confirm Password <span class="help-block">*</span></label>
                            <input id="password-confirm" type="password" class="form-control mb-1 req-f" name="password_confirmation" required>
                            <span class="help-block hidden req">Invalid password confirmation.</span>
                            @if($errors->has('password_confirmation'))
                                <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                            @endif
                        </div>

                        <div class="form-check my-3">
                            <input class="form-check-input mt-2" type="checkbox" value="" id="terms" required>
                            <label class="form-check-label font-weight-bold" for="terms" style="color: black; font-size: 12px;">I agree to the <a href="https://otcopenhouse.com/terms-conditions.html">Terms and Conditions</a></label>
                            <span class="help-block hidden" id="terms_error">You must agree to the terms and conditions.</span>
                        </div>

                        <button type="button" class="btn btn-danger form-control" id="register_submit">REGISTER NOW</button>
                        <button type="submit" class="btn btn-danger form-control hidden" id="complete_registration"></button>
                    </form>
                </div>
            </div>
        </div>
        <footer>
            <p class="pt-0">&copy; OTC Open House 2019</p>
        </footer>
    </div>
    <script>
        $(document).ready(function () {
            $('input[name="phone"]').mask('(###)-###-####');
            let csrf             = '{{csrf_token()}}';

            $('#register_submit').click(function () {
                let termsError = $('#terms_error');
                let canVerify = true;
                let requiredFields = $('.req-f');
                termsError.addClass('hidden');
                $('.help-block.req').addClass('hidden');
                $('#resend_verification,#cancel_verification').addClass('deactivated-no-events');
                requiredFields.each(function (index) {
                    if(!$(this).val() || $(this).val() == '') canVerify = false;
                });

                if(!document.getElementById('terms').checked)
                {
                    termsError.removeClass('hidden');
                    canVerify = false;
                }

                if(canVerify)
                {
                    let html =
                        '<div class="row" style="padding: 35px;">' +
                        '   <div class="col-sm-12">' +
                        '       <h1 id="verification_title">We have sent a verification code to the phone number provided.</h1>' +
                        '       <h4 id="verification_text">Please enter the code below to complete registration.</h4>' +
                        '   </div>' +
                        '   <div class="col-sm-12" style="margin-top: 30px; margin-left: 5px;">' +
                        '       <div style="width: 100%;">' +
                        '           <div id="resend_verification" class="deactivated-no-events">Resend Code</div>' +
                        '           <input class="m-auto" id="verification_code_input" type="text" name="verification_code">' +
                        '       </div>' +
                        '       <span style="display: block; width: 100%; margin-top: 60px;" class="help-block hidden" id="nex_failure"></span>' +
                        '   </div>' +
                        '   <div class="col-sm-12 row" style="margin-top: -10px; margin-left: 0.1px">' +
                        '       <button style="margin-top: 10px !important;" class="btn form-control m-auto deactivated-no-events" type="button" id="cancel_verification">Cancel <span class="timer"></span></button>' +
                        '       <button style="margin-top: 10px !important;" class="btn btn-danger form-control m-auto" type="button" id="submit_verification">Submit</button>' +
                        '   </div>' +
                        '</div>';
                    Swal2.fire({
                        html: html,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            let verifyNumberURL = '{{route('nexmo.verifyNumber')}}';
                            let phone           = $('input[name="phone"]').val();
                            let codeFailure     = $('#nex_failure');

                            let timer = 30;
                            let cancelResendText = $('#cancel_verification .timer, #resend_verification .timer');
                            setInterval(function () {
                                if (timer > 0)
                                {
                                    timer = timer - 1;
                                    cancelResendText.text(timer);
                                }
                                if(timer <= 0)
                                {
                                    cancelResendText.text('');
                                    $('#resend_verification,#cancel_verification').removeClass('deactivated-no-events');
                                }
                            },1000);


                            $.ajax({
                                url: verifyNumberURL,
                                method: 'POST',
                                data: {
                                    phone: phone,
                                    _token: csrf,

                                },
                                success: function (r) {
                                    if(r.status == 'fail')
                                    {
                                        let errors = $('#phone_errors');
                                        errors.text(r.message);
                                        errors.removeClass('hidden');
                                        Swal2.close();
                                    }
                                    else if(r.status == 'success')
                                    {
                                        $('input[name="request_id"]').val(r.data.request_id);
                                    }
                                    else if (r.status == 'skip')
                                    {
                                        $('#complete_registration').trigger('click');
                                    }
                                    else
                                    {
                                        Swal2.fire({
                                            title: 'Error',
                                            type: 'error',
                                            text: 'An unknown error has occurred, please try again.',
                                        });
                                    }
                                },
                                error: function (err) {
                                    console.log(err);
                                },
                            });

                            $('#submit_verification').click(function () {
                                let verification    = $('input[name="verification_code"]').val();
                                let request_id      = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                                let verifyURL       = '{{route('nexmo.verifyCode')}}';

                                console.log({
                                    verification: verification,
                                    request_id: request_id,
                                });

                                $.ajax({
                                    url: verifyURL,
                                    method: 'POST',
                                    data: {
                                        verification_code: verification,
                                        request_id: request_id,
                                        _token: csrf,

                                    },
                                    success: function (r) {
                                        codeFailure.addClass('hidden');
                                        console.log(r);
                                        if(r.status == 'fail')
                                        {
                                            if (r.message == 'The code provided does not match the expected value')
                                            {
                                                codeFailure.text('Incorrect verification code.');
                                                codeFailure.removeClass('hidden');
                                            }
                                        }
                                        else if (r.status == 'success')
                                        {
                                            $('#complete_registration').trigger('click');
                                        }
                                        else
                                        {
                                            Swal2.fire({
                                                title: 'Error',
                                                type: 'error',
                                                text: 'An unknown error has occurred, please try again.',
                                            });
                                        }

                                    },
                                    error: function (err) {
                                        console.log(err);
                                    },
                                });
                            });

                            $('#cancel_verification').click(function () {
                                let request_id       = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                                let cancelURL        = '{{route('nexmo.cancel')}}';

                                $.ajax({
                                    url: cancelURL,
                                    method: 'POST',
                                    data: {
                                        request_id: request_id,
                                        _token: csrf,

                                    },
                                    success: function (r) {
                                        console.log(r);
                                        if(r.status == 'success')
                                        {
                                            if (r.data.status == 0)
                                            {
                                                Swal2.close();
                                            }
                                        }
                                        else if(r.status == 'fail')
                                        {
                                            codeFailure.text(r.message);
                                            codeFailure.removeClass('hidden');
                                        }
                                        else
                                        {
                                            Swal2.fire({
                                                title: 'Error',
                                                type: 'error',
                                                text: 'An unknown error has occurred.',
                                            });
                                        }
                                    },
                                    error: function (err) {
                                        console.log(err);
                                    },
                                });
                            });

                            $('#resend_verification').click(function () {
                                let request_id  = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                                let phone       = $('input[name="phone"]').val();
                                let resendURL   = '{{route('nexmo.resend')}}';

                                $.ajax({
                                    url: resendURL,
                                    method: 'POST',
                                    data: {
                                        request_id: request_id,
                                        phone: phone,
                                        _token: csrf,

                                    },
                                    success: function (r) {
                                        if (r.status == 'success')
                                        {
                                            $('input[name="request_id"]').val(r.request_id);
                                            timer = 30;
                                            $('#resend_verification,#cancel_verification').addClass('deactivated-no-events');
                                            codeFailure.text('');
                                            codeFailure.addClass('hidden');
                                        }
                                        else if(r.status == 'fail')
                                        {
                                            codeFailure.text(r.message);
                                            codeFailure.removeClass('hidden');
                                        }
                                        else
                                        {
                                            Swal2.fire({
                                                title: 'Error',
                                                type: 'error',
                                                text: 'An unknown error has occurred.',
                                            });
                                        }
                                    },
                                    error: function (err) {
                                        console.log(err);
                                    },
                                });

                            });

                        },
                    });
                }
                else
                {
                    requiredFields.each(function (index) {
                        if(!$(this).val() || $(this).val() == '')
                            $(this).siblings('.help-block.req').removeClass('hidden');
                    });
                }

            });
        });
    </script>
</body>
@endsection