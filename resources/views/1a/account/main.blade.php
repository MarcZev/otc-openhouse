@extends('1a.layouts.master')
@section('content')
    <style>
        #profile_picture{
            max-height: 180px;
        }
    </style>
<body id="account-details" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row content-body mb-4">
            <div class="col-sm-6 profile-pic">
                <div id="special">
                    <center>
                        <a href="#">
                            <?php
                                $photo = \App\Http\Controllers\UserController::returnCorrectProfilePhoto();
                            ?>
                            @if($photo)
                                <div id="profile_picture" class="img-container" style="background-image: url('{{$photo}}'); display: inline-block;">

                                </div>
                            @else
                                <div id="profile_picture" class="img-container" style="background-image: url('{{asset('images/placeholder_180x180.jpg')}}'); display: inline-block;">

                                </div>
                            @endif
                            <button
                                    id="upload_profile_pic"
                                    type="button"
                                    class="btn"
                                    style="position: absolute;">
                                <i class="fas fa-camera"></i>
                            </button>
                        </a>
                    </center>
                </div>
            </div>
            <div class="col-sm-6 details pt-5">

                <h3 class="text-center mb-4">My Account</h3>

                <form action="{{route('user.update')}}" method="POST">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label id="caption" style="color: black;">Profile Details</label>
                        </div>
                        <input type="hidden" value="{{csrf_token()}}" name="_token">
                        <input type="hidden" name="photo" value="{{auth()->user()->photo ?? NULL}}">
                        <div class="col-sm-6">
                            <label for="first_name" class="mb-0">First Name</label><br>
                            <input value="{{auth()->user()->first_name ?? NULL}}" type="text" name="first_name" id="name" class="form-control" required>
                            @if($errors->has('first_name'))
                                <span class="help-block">{{$errors->first('first_name')}}</span>
                            @endif
                        </div>

                        <div class="col-sm-6">
                            <label for="last_name" class="mb-0">Last Name</label>
                            <input value="{{auth()->user()->last_name ?? NULL}}" type="text" name="last_name" id="name" class="form-control" required>
                            @if($errors->has('last_name'))
                                <span class="help-block">{{$errors->first('last_name')}}</span>
                            @endif
                        </div>

                        <div class="col-sm-12">
                            <label for="company_name" class="mb-0">Company Name</label>
                            <input value="{{auth()->user()->company_name}}" type="text" name="company_name" id="company_name" class="form-control" required>
                            @if($errors->has('company_name'))
                                <span class="help-block">{{$errors->first('company_name')}}</span>
                            @endif
                        </div>

                        <div class="col-sm-12">
                            <label for="email" class="mb-0">Phone</label>
                            <input maxlength="14" value="{{auth()->user()->phone}}" type="text" name="phone" id="phone" class="form-control" required>
                        </div>

                        <div class="col-sm-12">
                            <label for="email" class="mb-0">Email Address</label>
                            <input value="{{auth()->user()->email}}" type="email" name="email" id="email" class="form-control" required>
                            @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>

                        <div class="col-sm-6">
                            <label for="current_password" class="mb-0">Current Password</label>
                            <input type="password" name="current_password" id="current_password" class="form-control" required>
                            @if($errors->has('current_password'))
                                <span class="help-block">{{$errors->first('current_password')}}</span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label id="caption" style="color: black;">Change Password</label>
                        </div>
                        <div class="col-sm-6">
                            <label for="password" class="mb-0">New Password</label>
                            <input type="password" name="password" id="newpassword" class="form-control">
                            @if($errors->has('password'))
                                <span class="help-block">{{$errors->first('password')}}</span>
                            @endif
                            @if($errors->has('password_confirmation'))
                                <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                            @endif
                        </div>
                        <div class="col-sm-6 mb-3">
                            <label for="password_confirmation" class="mb-0">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="confirmpassword" class="form-control">
                        </div>
                        <button class="btn btn-danger form-control mx-3 mt-4 py-0">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            $('#phone').mask('(###)-###-####');
            $('#upload_profile_pic').click(function () {
                let filestackSignatureRoute = '{{route('filestack.signature')}}';
                $.ajax({
                    url: filestackSignatureRoute,
                    method: 'GET',
                    success: function (r) {
                        var policy      = r.policy;
                        var signature   = r.signature;
                        const security  = {
                            policy: policy,
                            signature: signature,
                        };

                        const clientOptions = {security};
                        const api_key = 'AzZlFYkLVRHKkQDAtOrYpz';
                        const client = filestack.init(api_key, clientOptions);
                        const options = {
                            maxFiles: 1,
                            accept: 'image/*',
                            uploadInBackground: false,
                            fromSources: [
                                'local_file_system',
                                'url',
                                'googledrive',
                                'dropbox',
                                'gmail',
                                'onedrive',
                                'onedriveforbusiness',
                                'clouddrive',
                                'box',
                            ],
                            onUploadDone: (res) =>
                            {
                                if(res.filesFailed.length > 0)
                                {
                                    Swal2.fire({
                                       title:   'Upload failed',
                                       text:    'Reference code: file-upload-001',
                                    });
                                }
                                if(res.filesUploaded.length > 0)
                                {
                                    let uploadedFile = res.filesUploaded[0];
                                    let url = uploadedFile.url + '?resize=width:180&policy='+policy+'&signature='+signature;
                                    $('input[name="photo"]').val(url);
                                    let profileImage = $('#profile_picture');
                                    profileImage.css('background-image','url(\''+url+'&resize=height:180'+'\')');
                                } //end of if uploadedFiles == 1
                            }, //end of onUploadDone
                        }; //end options

                        client.picker(options).open();
                    },
                    error: function () {
                        Swal2.fire({
                            title: 'Error',
                            type: 'error',
                            text: 'An unknown error has occurred.',
                        });
                    },
                });
            });
        });
    </script>
</body>
@endsection