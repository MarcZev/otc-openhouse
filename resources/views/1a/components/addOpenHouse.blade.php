<div class="row add mb-3 mt-1">
    <div class="col-sm-12">
        <div class="row add mb-3 mt-1">
            <div class="col-sm-12">
                <button
                        type="button"
                        class="btn btn-outline-danger form-control"
                        data-toggle="collapse"
                        href="#addOH"
                        role="button"
                        aria-expanded="false"
                        aria-controls="addOH"
                        id="addOH_trigger">
                    <i class="fas fa-plus" style="border:1px solid #D93149; border-radius: 360px; padding: 6px; margin-right: 6px; font-size: 10px;"></i> Add an Open House
                </button>
            </div>
            <?php
                $openDropDown = FALSE;
                if ($errors->has('start_time')  ||
                    $errors->has('end_time')    ||
                    $errors->has('type')        ||
                    $errors->has('property_id') ||
                    $errors->has('date'))
                {
                    $openDropDown = TRUE;
                }
            ?>
            <div class="col-sm-12 collapse" id="addOH">
                <div class="card add-open-house-card">
                    <div class="card-body px-4">
                        <form action="{{route('openHouse.add')}}" method="POST">
                            <input type="hidden" name="fromProperty" value="{{$fromProperty}}">
                            <div class="form-group row date">
                                <input type="hidden" value="{{csrf_token()}}" name="_token">
                                <div class="col-sm-6 px-3 py-1 mb-2">
                                    <label for="date">Open House Date</label>
                                    <input name="date" type="date" value="" id="date" required>
                                    @if($errors->has('date'))
                                        <span class="help-block">{{$errors->first('date')}}</span>
                                    @endif
                                </div>
                                <?php
                                    $times = config('constants.openHouseTimes');
                                ?>
                                <div class="col-sm-6 py-1 mb-2" style="border-radius: 4px; background-color: #eeeeee;">
                                    <label for="stime" >Start Time</label>
                                    <select name="start_time" class="open-house-add-time-dropdown">
                                        @foreach($times as $time)
                                            <option value="{{$time}}">{{$time}}</option>
                                        @endforeach
                                    </select>
                                    <label for="etime">End Time</label>
                                    <select name="end_time" class="open-house-add-time-dropdown">
                                        @foreach($times as $time)
                                            <option value="{{$time}}">{{$time}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('start_time'))
                                        <span class="help-block">{{$errors->first('start_time')}}</span>
                                    @endif
                                    @if($errors->has('end_time'))
                                        <span class="help-block">{{$errors->first('end_time')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="housetype" id="left" cl>Open House Type</label>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control" id="housetype" name="type" required>
                                        <option>Consumer Focused</option>
                                        <option>Broker Focused</option>
                                    </select>
                                    @if($errors->has('type'))
                                        <span class="help-block">{{$errors->first('type')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="paddress"  id="left">Property Address</label>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control" id="paddress" name="property_id">
                                        @foreach($properties as $property)
                                            <option value="{{$property->id}}">{{$property->address}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('property_id'))
                                        <span class="help-block">{{$errors->first('property_id')}}</span>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-danger"  id="left">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        let openDropDown = '{{$openDropDown}}' == '1';
        if(openDropDown) $('#addOH_trigger').trigger('click');
        let openHouseButton = document.getElementById('addOH_trigger');
        openHouseButton.addEventListener('click', function () {
            let open = this.getAttribute('aria-expanded') == 'false';
            if(open)
            {
                $(this).addClass('open-add-open-house-button');
                $(this).addClass('peach-background');
            }
            else
            {
                $(this).removeClass('open-add-open-house-button');
                $(this).removeClass('peach-background');
            }
        });

        $('#stime,#etime').timepicker({
            timeFormat: 'h:mm p',
            interval: 15,
            dynamic: true,
        });
    });
</script>