<nav class="navbar  navbar-light bg-white pl-4 py-2" id="topbar">
    <a class="navbar-brand" href="{{route('get.page', ['name' => 'home'])}}">
        <img src="{{asset('images/logo.png')}}" class="img-responsive">
    </a>
    <ul class="list-group list-group-horizontal">
        <?php
            $photo = \App\Http\Controllers\UserController::returnCorrectProfilePhoto();
        ?>
        @if(!$photo)
        <li class="list-group-item p-0"  style="border:none;">
            <div class="dropdown dropleft">
                <button type="button" class="btn p-0 mr-3" id="btn1" data-toggle="dropdown">
                    <img src="{{asset('images/button2.png')}}">
                </button>
                <div class="dropdown-menu">
                    <a href="{{route('get.page', ['name' => 'account'])}}" class="dropdown-item">My Account</a>
                    <a href="{{route('logout')}}" class="dropdown-item text-left">Sign Out</a>
                </div>
            </div>
        </li>
        @else
        <li class="list-group-item p-0"  style="border:none;">
            <div class="dropdown dropleft ml-0" id="drop">
                <button  type="button" class="btn" data-toggle="dropdown" id="btn2">
                    <img src="{{$photo}}" class="img-responsive user_profile_picture">
                </button>
                <div class="dropdown-menu">
                    <a href="{{route('get.page', ['name' => 'account'])}}" class="dropdown-item">My Account</a>
                    <a href="{{route('logout')}}" class="dropdown-item text-left">Sign Out</a>
                </div>
            </div>
        </li>
        @endif
    </ul>
</nav>
<nav class="navbar navbar-expand-lg navbar-light px-0 mt-0">
    <button id="sidebarmenu" class="navbar-toggler mt-0" type="button" data-toggle="collapse" data-target="#sidebar" aria-controls="sidebar"  aria-label="Toggle navigation">
        <i class="fas fa-bars"></i> MENU
    </button>

    <div id="sidebar" class="collapse navbar-collapse">

        <ul class="navbar-nav flex-column mr-4">
            @if($page == 'home') <li class="nav-item highlighted-menu-item">
            @else <li class="nav-item">
            @endif
                <a href="{{route('get.page', ['name' => 'home'])}}" class="nav-link"><i class="fas fa-home"></i> Home</a>
            </li>
            <!--
            @if($page == 'clients') <li class="nav-item highlighted-menu-item">
            @else <li class="nav-item">
            @endif
                <a href="route('get.page', ['name' => 'clients'])}}" class="nav-link"><i class="fas fa-heart"></i> My Clients</a>
            </li>
            -->
            @if($page == 'properties') <li class="nav-item highlighted-menu-item">
            @else <li class="nav-item">
            @endif
                <a href="{{route('get.page', ['name' => 'properties'])}}" class="nav-link"><i class="fas fa-edit"></i> Properties</a></a>
            </li>
            @if($page == 'openHouses') <li class="nav-item highlighted-menu-item">
            @else <li class="nav-item">
            @endif
                <a href="{{route('get.page', ['name' => 'openHouses'])}}" class="nav-link"><i class="fas fa-ticket-alt"></i> Open Houses</a>
            </li>
            @if($page == 'leads') <li class="nav-item highlighted-menu-item">
            @else <li class="nav-item">
            @endif
                <a href="{{route('get.page', ['name' => 'leads'])}}" class="nav-link"><i class="fas fa-user-circle"></i> Leads</a>
            </li>
            <li class="nav-item" disabled>
                <a href="#" class="nav-link" style="font-size: 14px; color: #B4B4B4;"><i class="fas fa-cog"></i> CRM Integrations <span class="badge badge-danger badge-pill" style="font-size: 10px;">COMING SOON</span></a>
            </li>
        </ul>
    </div>
</nav>