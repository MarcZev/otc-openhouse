@extends('1a.layouts.master')
@section('content')
    <body id="my-clients-details" class="main-layout">
    <div class="container-fluid content py-3 mt-2">
        <div class="row py-4 content-body">

            <div class="col-sm-12" id="main-col">
                <h3 class="text-center mb-4">Add Client</h3>
                <form action="{{route('client.add')}}" method="POST">
                    <div class="form-group-row">
                        <div class="text-center col-sm-12">
                            <button class="btn" id="save" type="submit">
                                <img id="modeImg" src="{{asset('images/save.png')}}" ><br><span id="mode">Save</span>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row px-3 mt-4">
                        <input name="_token" value="{{csrf_token()}}" type="hidden">
                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <label for="NameFirst" class="float-left mb-1">First Name</label>
                                <input name="NameFirst" type="text" class="form-control"
                                       id="NameFirst" required value="{{old('NameFirst') ?? NULL}}">
                                @if($errors->has('NameFirst'))
                                    <span class="help-block">{{$errors->first('NameFirst')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="NameLast" class="float-left mb-1">Last Name</label>
                                <input name="NameLast" type="text" class="form-control"
                                       id="NameLast" value="{{old('NameLast') ?? NULL}}">
                                @if($errors->has('NameLast'))
                                    <span class="help-block">{{$errors->first('NameLast')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Phone Number</label>
                                <input maxlength="14" name="PhonePrimary" type="text" class="form-control"
                                       id="lname" value="{{old('PhonePrimary') ?? NULL}}">
                                @if($errors->has('PhonePrimary'))
                                    <span class="help-block">{{$errors->first('PhonePrimary')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Additional Phone Number</label>
                                <input maxlength="14" name="PhoneSecondary" type="text" class="form-control"
                                       id="lname" value="{{old('PhoneSecondary') ?? NULL}}">
                                @if($errors->has('PhoneSecondary'))
                                    <span class="help-block">{{$errors->first('PhoneSecondary')}}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <label for="EmailPrimary" class="float-left mb-1">Email Address</label>
                                <input name="EmailPrimary" type="email" class="form-control"
                                       id="email" required value="{{old('EmailPrimary') ?? NULL}}">
                                @if($errors->has('EmailPrimary'))
                                    <span class="help-block">{{$errors->first('EmailPrimary')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="EmailSecondary" class="float-left mb-1">Additional Email Address</label>
                                <input name="EmailSecondary" type="email" class="form-control"
                                       id="email" value="{{old('EmailSecondary') ?? NULL}}">
                                @if($errors->has('EmailSecondary'))
                                    <span class="help-block">{{$errors->first('EmailSecondary')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="PropertyVisited" class="float-left mb-1">Property Visited</label>
                                <input name="PropertyVisited" type="text" class="form-control"
                                       id="property" value="{{old('PropertyVisited') ?? NULL}}">
                                @if($errors->has('PropertyVisited'))
                                    <span class="help-block">{{$errors->first('PropertyVisited')}}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function ()
        {
            $('input[name="PhonePrimary"],input[name="PhoneSecondary"]').mask('(###)-###-####');
        });
    </script>
    </body>
@endsection