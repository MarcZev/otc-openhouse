@extends('1a.layouts.master')
@section('content')
<body id="my-clients-details" class="main-layout">
    <div class="container-fluid content py-3 mt-2">
        <div class="row py-4 content-body">

            <div class="col-sm-12" id="main-col">
                <form action="" method="POST">
                    <div class="form-group-row" style="max-width: 250px; margin: 0 auto;">
                        <div class="col-sm-12 p-0">
                            <input type="text" name="name" value="{{$data['Name']}}" disabled class="form-control" id="name">
                        </div>
                    </div>
                    <div class="form-group-row">
                        <div class="text-center col-sm-12">
                            <button class="btn" id="editMode" type="button">
                                <img id="modeImg" src="{{asset('images/edit.png')}}" ><br><span id="mode">Edit</span>
                            </button>
                            <button class="btn hidden" id="cancel" type="button">
                                <img src="{{asset('images/cancel.png')}}" ><br><span>Cancel</span>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row px-3 mt-4">

                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Phone Number</label>
                                <input type="text" class="form-control" id="lname" disabled required value="{{$data['PrimaryPhone'] ?? NULL}}">
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Additional Phone Number</label>
                                <input type="text" class="form-control" id="lname" disabled required value="{{$data['SecondaryPhone'] ?? NULL}}">
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Email Address</label>
                                <input type="email" class="form-control" id="email" disabled required value="{{$data['PrimaryEmail'] ?? NULL}}">
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Additional Email Address</label>
                                <input type="email" class="form-control" id="email" disabled required value="{{$data['SecondaryEmail'] ?? NULL}}">
                            </div>
                        </div>

                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <label for="property" class="float-left mb-1">Property Visited</label>
                                <input type="text" class="form-control" id="property" disabled required value="{{$data['PropertyVisited']}}">
                            </div>
                            <?php
                                $upcomingOpenHouses = [
                                    [
                                        'date'      => 'Sunday, March 9',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ],
                                    [
                                        'date'      => 'Monday, March 15',
                                        'startTime' => '2PM',
                                        'endTime'   => '3PM',
                                    ],
                                    [
                                        'date'      => 'Tuesday, March 16',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ],
                                    [
                                        'date'      => 'Tuesday, March 16',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ],
                                    [
                                        'date'      => 'Tuesday, March 16',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ],
                                    [
                                        'date'      => 'Tuesday, March 16',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ],
                                    [
                                        'date'      => 'Tuesday, March 16',
                                        'startTime' => '2PM',
                                        'endTime'   => '4PM',
                                    ]
                                ];
                            ?>
                            <div class="col-sm-12 mb-0 pt-3">
                                <h6>Upcoming Open Houses</h6>
                                <a id="showAll">Show All</a>
                            </div>

                            <div class="col-sm-12 mb-3 pt-3" id="group-buttons">
                                @foreach($upcomingOpenHouses as $openHouse)
                                    @if($loop->index < 3)
                                        <div class="open-house-card btn-group btn-block">
                                            <button type="button" class="btn  form-control btn1">{{$openHouse['date']}}</button>
                                            <button type="button" class="btn  form-control btn2"><i class="far fa-clock"></i>
                                                {{$openHouse['startTime']}} - {{$openHouse['endTime']}}
                                            </button>
                                        </div>
                                    @else
                                        <div class="open-house-card btn-group btn-block hidden">
                                            <button type="button" class="btn  form-control btn1">{{$openHouse['date']}}</button>
                                            <button type="button" class="btn  form-control btn2"><i class="far fa-clock"></i>
                                                {{$openHouse['startTime']}} - {{$openHouse['endTime']}}
                                            </button>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                        </div>

                    </div>

                    <div class="form-group-row px-3" id="action-btn">
                        <div class="col-sm-12">
                            <a href="tel:1+{{$data['PrimaryPhone']}}">
                                <button class="btn">
                                    <img src="{{asset('images/icon-phone-2.png')}}"><br> Call
                                </button>
                            </a>
                            <a href="mailto:{{$data['PrimaryEmail']}}">
                                <button class="btn">
                                    <img src="{{asset('images/icon-email-2.png')}}"><br> Send An Email
                                </button>
                            </a>
                            <a href="#">
                                <button class="btn">
                                    <img src="{{asset('images/icon-text-msg.png')}}"><br> Send a Text Message
                                </button>
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function ()
        {
            $('#showAll').click(function () {
                $('.open-house-card').removeClass('hidden');
                $(this).hide();
            });
            let editImg = '{{asset('images/edit.png')}}';
            let saveImg = '{{asset('images/save.png')}}';
            $('#editMode').click(function () {
                let self = $(this);
                self.children('img').attr('src', saveImg);
                self.prop('id', 'save');
                self.children('#mode').text('Save');
                self.siblings('#cancel').show();
                $('input').each(function () {
                    $(this).attr('disabled',false);
                });
            });
            $('#save').click(function () {
                //todo save the form
            });
            $('#cancel').click(function () {
                location.reload();
            });
        });
    </script>
</body>
@endsection