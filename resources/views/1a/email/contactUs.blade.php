<div>
    <h1>Contact Us Email From OTC Open House Web App</h1>

    <h3>Name: {!! $name ?? NULL !!}</h3>
    <h3>Email: {!! $email ?? NULL !!}</h3>
    <h3>Subject: {!! $subject ?? NULL !!}</h3>
    <h3>Message: {!! $messageText ?? NULL !!}</h3>

</div>