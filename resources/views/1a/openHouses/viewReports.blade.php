@extends('1a.layouts.master')
@section('custom_css')
    <?php
    $degrees = 18*(int)round($rating);
    ?>
    <style>
        #progress_circle{
            position: absolute;
            top: 10px;
            left: 10px;
        }

        .circle-wrap {
            position: absolute;
            top: 10px;
            left: 20px;
            margin: 20px auto;
            width: 150px;
            height: 150px;
            background: #e6e2e7;
            border-radius: 50%;
        }

        .circle-wrap .circle .mask,
        .circle-wrap .circle .fill {
            width: 150px;
            height: 150px;
            position: absolute;
            border-radius: 50%;
        }

        .circle-wrap .circle .mask {
            clip: rect(0px, 150px, 150px, 75px);
        }

        .circle-wrap .circle .mask .fill {
            clip: rect(0px, 75px, 150px, 0px);
            background-color: #CE343E;
        }

        .circle-wrap .inside-circle {
            width: 130px;
            height: 130px;
            border-radius: 50%;
            background: #fff;
            text-align: center;
            margin-top: 10px;
            margin-left: 10px;
            padding-top: 30px;
            position: absolute;
            z-index: 1;
            font-weight: 700;
            font-size: 2em;
            line-height: 13px;
            color: #CE343E;
        }

        .circle-wrap .inside-circle span{
            font-size: 10px;
            line-height: 0;
            color: #B9B9B9;
        }
/*
        .circle-wrap .circle .mask {
            clip: rect(0px, 150px, 150px, 75px);
        }

        .circle-wrap .circle .mask .fill {
            clip: rect(0px, 75px, 150px, 0px);
            background-color: #CE343E;
        }
*/
        .circle-wrap .circle .mask.full,
        .circle-wrap .circle .fill {
            /*animation: fill ease-in-out 3s;*/
            transform: rotate({{$degrees}}deg);
        }

        /*
        Need to overwrite row margins.
         */
        .row{
            margin-left: 0;
            margin-right: 0;
        }
    </style>
@endsection
@section('content')
<!--<body id="view-leads" class="main-layout">-->
<body id="add-new-property" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-0 content-body">
            <div class="col-sm-6 slider py-3" id="uploaded_image" style="background: {!! $photo ? 'url(\''.$photo.'\')' : asset('images/map_cropped.jpg') !!}; background-repeat: no-repeat; background-size: cover">
                <div class="circle-wrap">
                    <div class="circle">
                        <div class="mask full">
                            <div class="fill" data-progress="10%"></div>
                        </div>
                        <div class="mask half">
                            <div class="fill"></div>
                        </div>
                        <div class="inside-circle"> {{(int)round($rating)}}/10<br><br><span>Avg Rating<br> people's interest<br> in the property </span> </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 details row" style="padding: 0">
                @if($openHouseCount > 1)
                    @if($minOpenHouseDate == $maxOpenHouseDate)
                        <h6 class="text-center mb-4 open-houses-report-header col-sm-12">Report for Open Houses on</h6>
                        <h4 class="open-houses-report-dates col-sm-12">{{date('F d Y', $minOpenHouseDate)}}</h4>
                    @else
                        <h6 class="text-center mb-4 open-houses-report-header col-sm-12">Report for Open Houses from</h6>
                        <h4 class="open-houses-report-dates col-sm-12">{{date('F d Y', $minOpenHouseDate)}} to {{date('F d Y', $maxOpenHouseDate)}}</h4>
                    @endif
                @else
                    <h6 class="text-center mb-4 open-houses-report-header col-sm-12">Report for Open House On</h6>
                    <h4 class="open-houses-report-dates col-sm-12">{{date('F d Y', $minOpenHouseDate)}}</h4>
                @endif
                <h2 class="text-center col-sm-12">{{$address}}</h2>
                <div class="report-stat-row" style="width: 100%;">
                    <div class="report-checkin-checkout-stats row text-center">
                        <div class="col-md-12 report-checkins-checkouts-header">CHECK INS</div>
                        <div class="col-md-12 report-checkins-checkouts-stat">{{$signIn}}</div>
                    </div><div class="report-checkin-checkout-stats row text-center" style="border-left: solid 1px white;">
                        <div class="col-md-12 report-checkins-checkouts-header">CHECK OUTS</div>
                        <div class="col-md-12 report-checkins-checkouts-stat">{{$signOut}}</div>
                    </div>
                </div>
                <div class="col-sm-12 row report-stat-row" style="margin-top: 20px; margin-left: 5px;">
                    <div class="col-sm-4 text-center report-stat-section">
                        <img src="{{asset('images/handshake.png')}}" class="img-responsive report-stats-image">
                        <div class="report-stats-value report-red-color">{{$lookingToBuy}}</div>
                        <div class="report-stats-descriptor-text">people looking to buy a home ASAP</div>
                    </div>
                    <div class="col-sm-4 text-center report-stat-section">
                        <img src="{{asset('images/icon-leads.png')}}" class="img-responsive report-stats-image">
                        <div class="report-stats-value report-red-color">{{$workingWithAgent}}</div>
                        <div class="report-stats-descriptor-text">people currently working with an agent</div>
                    </div>
                    <div class="col-sm-4 text-center report-stat-section">
                        <img src="{{asset('images/pre-approved-mortgage.png')}}" class="img-responsive report-stats-image">
                        <div class="report-stats-value report-red-color">{{$preApprovedMortgage}}</div>
                        <div class="report-stats-descriptor-text">people pre-approved for a mortgage</div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-sm-12 row report-stat-row" style="margin-top: 10px; margin-left: 5px; margin-bottom: 30px;">
                    <div class="text-center col-sm-12"
                         style="margin-bottom: -10px;
                         margin-top: -10px;"><p class="report-price-of-property-header">Users say the price of the property is</p></div>
                    <div class="col-sm-4 text-center report-stat-section">
                        <div class="report-stats-value" style="color:#336aa6">{{$priceLow}}</div>
                        <div class="report-stats-descriptor-text">Too Low</div>
                    </div>
                    <div class="col-sm-4 text-center report-stat-section">
                        <div class="report-stats-value" style="color: #aedd64">{{$priceRight}}</div>
                        <div class="report-stats-descriptor-text">The Price is Right, Bob!</div>
                    </div>
                    <div class="col-sm-4 text-center report-stat-section" style="margin-bottom: 40px;">
                        <div class="report-stats-value" style="color: #d6cf37;">{{$priceHigh}}</div>
                        <div class="report-stats-descriptor-text">Too High</div>
                    </div>
                </div>
                <form method="POST" action="{{route('property.report')}}" target="_blank">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="data" value="{{json_encode($__data)}}">
                    <button type="submit" class="btn btn-danger form-control report-print-report-button">
                        Print Report
                    </button>
                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <!--
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-2 content-body px-0 mb-0">
            <div class="col-sm-12">
                <h2 class="text-center py-3">$address}}</h2>
                <div class="col-sm-12 m-auto text-center">
                    @if($photo)
                        <img src="$photo}}" class="img-responsive" style="max-height: 300px;">
                    @endif
                </div>
                <h6>Average Rating: $rating}}</h6>
                <h6>Check Ins: $signIn}}</h6>
                <h6>Check Outs: $signOut}}</h6>
                <h6>People looking to buy ASAP: $lookingToBuy}}</h6>
                <h6>People currently working with an agent: $workingWithAgent}}</h6>
                <h6>People pre-approved for a mortgage: $preApprovedMortgage}}</h6>

                <div class="col-sm-12 row">
                    <div class="col-sm-6">
                        @if(!isServerLive())
                            <button id="full_details_report" class="btn btn-danger form-control deactivated-no-events">Get Full Details via Email</button>
                        @else
                            <button id="full_details_report" class="btn btn-danger form-control">Get Full Details via Email</button>
                        @endif
                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    -->
    <script>
        $(document).ready(function () {
            $('#full_details_report').click(function () {
                let route = '{{route('openHouses.emailReports', ['propertyID' => $propertyID, 'ids' => $ids])}}';
                Swal2.fire({
                    text: 'Sending report...',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                $.ajax({
                    url: route,
                    method: 'GET',
                    success: function (r) {
                        if(r.status == 'success')
                        {
                            Swal2.fire({
                                text: 'Email successfully sent!',
                                toast: true,
                                type: 'success',
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                            });
                        }
                        else if (r.status == 'fail')
                        {
                            Swal2.fire({
                                title: 'Error',
                                text: 'An unknown error has occurred, please try again.',
                                type: 'error',
                            });
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    },
                });
            });
        });
    </script>
    </body>
@endsection