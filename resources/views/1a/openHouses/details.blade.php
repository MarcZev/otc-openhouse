@extends('1a.layouts.prelogin')
@section('content')
    <body id="Open_House_Details" class="open-house">
    <!-- Banner -->
    <div class="container-fluid banner">
        <a href="{{route('logout.user', ['url' => \App\Library\Utilities\_Crypt::base64url_encode(route('get.page', ['name' => 'openHouses']))])}}"><button class="btn" id="close">X</button></a>
    </div>
    <!-- Body Content -->
    <div class="container-fluid content pt-4">
        <center>
            <h1>Welcome to <br>{{$data['address']}}</h1>
            <p class="pb-4 pt-2" id="p1">Agent: {{$data['agent_name']}}</p>

            <a href="{{route('openHouse.signIn', ['id' => $data['id']])}}">
                <button class="btn btn-danger mr-3" id="btn-in">Sign In</button>
            </a>
            <a href="{{route('openHouse.signOut', ['id' => $data['id']])}}">
                <button class="btn btn-outline-danger" id="btn-out">Sign Out</button>
            </a>

            <div class="clearfix details mt-5 mb-4">
                <div class="float-left mx-2" id="details-list">
                    <img src="{{asset('images/icon-ohd-price.png')}}" class="float-left mr-2">
                    <p class="float-left">List Price <br>${{number_format($data['price'])}}</p>
                </div>
                <div class="float-left mx-2" id="details-list">
                    <img src="{{asset('images/icon-ohd-type.png')}}" class="float-left mr-2">
                    <p class="float-left">Property Type<br>{{$data['type']}}</p>
                </div>
                <div class="float-left mx-2" id="details-list">
                    <img src="{{asset('images/icon-ohd-footage.png')}}" class="float-left mr-2">
                    <p class="float-left">Square Feet<br>{{number_format($data['square_footage'])}} sq ft</p>
                </div>
                <div class="float-left mx-2" id="details-list">
                    <img src="{{asset('images/icon-ohd-rooms.png')}}" class="float-left mr-2">
                    <p class="float-left">Number of Bedrooms<br>{{$data['bedrooms']}}</p>
                </div>
                <div class="float-left mx-2" id="details-list">
                    <img src="{{asset('images/icon-ohd-bath.png')}}" class="float-left mr-2">
                    <p class="float-left">Number of Bathrooms<br>{{$data['bathrooms']}}</p>
                </div>
            </div>
        </center>
    </div>

    <!-- Footer -->
    <footer class="pt-5 pb-2">
        <p>&copy; OTC Open House 2019</p>
    </footer>
    </body>
@endsection