@extends('1a.layouts.master')
@section('content')
    <body id="open-houses" class="main-layout">
    <div class="container-fluid content p-3 mt-5">
        <div class="row content-body" id="open-houses-for-property">
            <div class="col-sm-12">
                <h2 class="text-center pb-4">Open Houses for {{$data['address']}}</h2>
                <?php
                $upcomingOpenHouses = $data['openHouses']['upcoming'];
                $pastOpenHouses = $data['openHouses']['pastDue'];
                $properties = [
                    (object) array(
                        'id'        => $data['id'],
                        'address'   => $data['address'],
                    ),
                ];
                ?>
                @include('1a.components.addOpenHouse', [
                    'fromProperty' => TRUE,
                ])
                <div class="row past-events">

                    @if(count($upcomingOpenHouses) > 0)
                        <div class="col-sm-12 pb-3">
                            <h6>Upcoming Open Houses</h6>
                        </div>
                    @endif
                    @foreach($upcomingOpenHouses as $openHouse)
                        @if($loop->index < 3) <div class="upcoming-openHouse-card col-sm-4 mb-3">
                        @else <div class="upcoming-openHouse-card col-sm-4 mb-3 hidden">
                        @endif
                                <div class="card">
                                    <div class="card-header text-center">
                                        <input value="{{$openHouse['id']}}" type="checkbox" class="choose_open_house_checkbox hidden" name="openHouses[]">
                                        {{$data['address']}}
                                    </div>
                                    <div class="card-body py-2 date upcoming-events-date">
                                        <p class="card-text text-center">
                                            {{date('l, F d Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    @php
                                        $checkIns   = count($openHouse->checkIns);
                                        $checkOuts  = count($openHouse->checkOuts);
                                        $comments   = count($openHouse->comments);
                                    @endphp
                                    @if($checkIns || $checkOuts || $comments)
                                        <div class="card-body activity text-center px-2 pt-3 pb-0">
                                            <p class="card-text">
                                                <img src="{{asset('images/icon-check-in.png')}}"><br>
                                                <span class="num">{{count($openHouse->checkIns)}}</span><br>
                                                <span class="name">Check-ins</span>
                                            </p>
                                            <p class="card-text">
                                                <img src="{{asset('images/icon-check-out.png')}}"><br>
                                                <span class="num">{{count($openHouse->checkOuts)}}</span><br>
                                                <span class="name">Check-outs</span>
                                            </p>
                                            <p class="card-text">
                                                <img src="{{asset('images/icon-text-msg-2.png')}}" style="width: 20px;"><br>
                                                <span class="num">{{count($openHouse->comments)}}</span><br>
                                                <span class="name">Comments</span>
                                            </p>
                                            <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link"></a>
                                        </div>
                                    @else
                                        <div class="card-body upcoming-events-activity text-center">
                                            <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link">
                                                No activity yet
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                    @endforeach
                </div>

                <div class="row past-events">
                    @if(count($pastOpenHouses) > 0)
                        <div class="col-sm-12 pb-3">
                            <h6>Past Open Houses</h6>
                        </div>
                    @endif
                    @foreach($pastOpenHouses as $openHouse)
                        @if($loop->index < 3) <div class="past-openHouse-card col-sm-4 mb-3">
                            @else <div class="past-openHouse-card col-sm-4 mb-3 hidden">
                                @endif
                                <div class="card">
                                    <div class="card-header text-center">
                                        <input value="{{$openHouse['id']}}" type="checkbox" class="choose_open_house_checkbox hidden" name="openHouses[]">
                                        {{$data['address']}}
                                    </div>
                                    <div class="card-body py-2 date">
                                        <p class="card-text text-center">
                                            {{date('l, F d Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    <div class="card-body activity text-center px-2 pt-3 pb-0">
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-in.png')}}"><br>
                                            <span class="num">{{count($openHouse->checkIns)}}</span><br>
                                            <span class="name">Check-ins</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-out.png')}}"><br>
                                            <span class="num">{{count($openHouse->checkOuts)}}</span><br>
                                            <span class="name">Check-outs</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-text-msg-2.png')}}" style="width: 20px;"><br>
                                            <span class="num">{{count($openHouse->comments)}}</span><br>
                                            <span class="name">Comments</span>
                                        </p>
                                        <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link"></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        @if(count($upcomingOpenHouses) > 0 || count($pastOpenHouses) > 0)
                            <div class="row choose mt-2">
                                <div class="col-sm-12 mb-3 m-auto">
                                    <button type="button" class="btn form-control" id="view_leads_for_event"><img src="{{asset('images/icon-leads-2.png')}}"> Choose Events to View Leads or Reports</button>
                                </div>
                                <div class="col-sm-6 mb-3">
                                    <button style="margin-top: 10px;" type="button" class="btn form-control hidden" id="execute_view_leads">View Leads</button>
                                </div>
                                <div class="col-sm-6 mb-3">
                                    <button style="margin-top: 10px;" type="button" class="btn form-control hidden" id="execute_view_reports">View Reports</button>
                                </div>
                            </div>
                        @endif

                        <hr class="mb-2" style="">

                        <center><a id="more" class="view-more">VIEW MORE OPEN HOUSES</a></center>

                </div>
            </div>
            <p class="my-3 footer">&COPY; OTC Open House 2019</p>
        </div>
        <script>
            $(document).ready(function () {
                let openHouses      = [];
                let moreElements    = new showMoreElements('past-openHouse-card', 3);
                let moreElements2   = new showMoreElements('upcoming-openHouse-card', 3);
                $('#more').click(function () {
                    moreElements.viewMore(3);
                    moreElements2.viewMore(3);
                });

                $('.choose_open_house_checkbox').click(function () {
                    $('input:checked').map(function () {
                        openHouses.push($(this).val());
                    });
                    if (openHouses.length > 0) $('#execute_view_leads,#execute_view_reports').removeClass('hidden');
                    else $('#execute_view_leads,#execute_view_reports').addClass('hidden');
                });

                $('#execute_view_leads,#execute_view_reports').click(function () {
                    let openHouses = [];
                    $('input:checked').map(function () {
                        openHouses.push($(this).val());
                    });

                    if($(this).is('#execute_view_leads'))
                    {
                        let route = '{{route('openHouses.viewLeads', ['ids' => ':oh'])}}';
                        route = route.replace(':oh',openHouses.join(','));
                        window.location.href = route;
                    }
                    else if ($(this).is('#execute_view_reports'))
                    {
                        let route = '{{route('openHouses.viewReports', ['propertyID' => $data['id'], 'ids' => ':oh'])}}';
                        route = route.replace(':oh',openHouses.join(','));
                        window.location.href = route;
                    }

                });

                $('#view_leads_for_event,#generate_reports_for_events').click(function () {
                    $('input:checked').map(function () {
                        this.checked = false;
                    });
                    $('.choose_open_house_checkbox').removeClass('hidden');
                });

            });
        </script>
    </body>
@endsection