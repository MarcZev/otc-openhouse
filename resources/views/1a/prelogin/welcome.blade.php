<html>
<head>
    <title>Open House App and Management Platform</title>
    <meta name="keywords" content="Open House Service, Open House App, OTC Open House, OTCOpenHouse.com, real estate assistant, offer to close, offertoclose.com" />
    <meta name="description" content="OTC Open House is a mobile app to help make the open house a better tool for agents to track their properties, open houses, and leads created from their open house." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <!-- Bootstrap for responsive layout-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap_v3.3.7.min.css')}}">

    <!-- Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin.css')}}">

    <!-- Scripts -->
    <script src="{{asset('js/jquery_v1.11.2.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Fonts CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
          crossorigin="anonymous">

</head>

<body>

<div class="navbar  ">
    <div class="container">
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img src="{{asset('images/prelogin_logo.png')}}"></a>

            <button class="navbar-toggle collapse_btn" data-toggle="collapse" data-target=".nav1">
                <span class="icon-bar" ></span>
                <span class="icon-bar" ></span>
                <span class="icon-bar" ></span>
            </button>
        </div>

        <div class="collapse navbar-collapse nav1 ">
            <ul class="nav navbar-nav">
                <li><a href="#About_Us">ABOUT US</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" id="dropdownProducts" data-toggle="dropdown">OUR PRODUCTS</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownProducts">
                        <li><a href="https://www.offertoclose.com/">Transaction Management</a></li>
                        <li><a href="https://www.offertoclose.com/offers">Offers App</a></li>
                        <li><a href="https://www.offertoclose.com/transaction-timeline">Transaction Timeline</a></li>
                    </ul>
                </li>
                <li><a href="#Contact_Us">CONTACT US</a></li>
                <li><a href="{{route('register')}}">REGISTER</a></li>
                <li><a href="{{route('login')}}">SIGN IN</a></li>
                <li><a href="tel:8336826736" style="color: #CE343E;">(833) OTC-OPEN</a></li>
            </ul>
        </div>
    </div>
</div>

<section class="container-fluid" id="what">
    <div class="container">
        <div class="row" style="max-width: 1000px; margin: 0 auto;">
            <div class="col-sm-4">
                <img src="{{asset('images/prelogin_what-mob.png')}}" class="img-responsive" style="max-width: 400px; max-height: 500px">
            </div>
            <div class="col-sm-8" style="padding: 70px 40px 30px; ">
                <h1>OTC Open House App</h1>
                <p>OTC Open House is an open house app and management platform designed to make the open house a better tool for agents to track their properties, open houses and leads created from their open house. It uses a simple web interface and native mobile application that keep track of your properties, open houses, and open house leads. Unlike traditional means of gathering information, this platform will allow customers or agents to input information directly into the system for better organization and follow-up.</p>
                <button class="btn" type="button"><img src="{{asset('images/appstore.png')}}" class="img-responsive"></button>
                <button class="btn" type="button"><img src="{{asset('images/googleplay.png')}}" class="img-responsive"></button>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid" id="how">
    <div class="container">
        <h1 class="text-center">How it Works</h1>
        <div class="row" style="max-width: 1100px; margin: 0 auto;">
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-1.png')}}" class="img-responsive">
                    <p>Track open<br>house leads</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-2.png')}}" class="img-responsive">
                    <p>Fast and effective reporting for each open house</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-3.png')}}" class="img-responsive">
                    <p>Get real-time feedback from open house visitors</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-4.png')}}" class="img-responsive">
                    <p>Simple follow-up with email, text, or phone calls</p>
                </center>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-7" style="padding-right: 0;"><a id="About_Us"> </a>
                <h2>About Offer To Close</h2>
                <p>At Offer To Close, our mission is to create tools and services that make the home-buying process simple, transparent, and affordable and that extends to making the entire open house experience simpler and more efficient.  We created OTC Open House to add one more arrow in the agent&#39;s quiver while also increasing the value proposition that we uniquely offer agents with our transaction management platform, experienced transaction coordinators, and tech services like OTC Open House <br><br>OTC Open House apps will be available to download soon for iOS on Apple&#39;s&reg; App Store and for Android&trade; users on the Google Play&trade; Store.</p>
                <button class="btn" type="button"><img src="{{asset('images/appstore.png')}}" class="img-responsive"></button>
                <button class="btn" type="button"><img src="{{asset('images/googleplay.png')}}" class="img-responsive"></button>
            </div>
            <div class="col-sm-5" style="padding-top: 150px; padding-left: 0px;">
                <img src="{{asset('images/prelogin_about-mob.png')}}" class="img-responsive">
            </div>
        </div>
    </div>
</section>

<section class="container-fluid" id="contact">
    <div class="container"><a id="Contact_Us"> </a>
        <h1>Contact Us</h1>
        <div class="row" style="max-width: 1100px; margin: 0 auto;">
            <div class="col-sm-6">
                <form>
                    <input type="text" name="uname" placeholder="Name" class="form-control">
                    <span class="help-block uname hidden">&nbspName is required.</span>
                    <input type="email" name="email" placeholder="Email"  class="form-control">
                    <span class="help-block email hidden">&nbspEmail is required.</span>
                    <input type="text" name="subject" placeholder="Subject"  class="form-control">
                    <span class="help-block subject hidden">&nbspSubject is required.</span>
                    <textarea name="messageText" placeholder="Message" class="form-control"></textarea>
                    <span class="help-block messageText hidden">&nbspMessage is required.</span>
                    <button class="btn submit_mail" type="button" class="form-control"><img src="{{asset('images/prelogin_send.png')}}" class="img-responsive"></button>
                    <input type="submit" class="submit_mail">
                </form>
            </div>
            <div class="col-sm-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3300.9546540313286!2d-118.56103578449382!3d34.17307701879943!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2994078f82e73%3A0x9d7aa561b892e88b!2s19525+Ventura+Blvd%2C+Tarzana%2C+CA+91356!5e0!3m2!1sen!2sus!4v1553638496921" width="500" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<footer>
    <center>
        <img src="{{asset('images/prelogin_logo2.png')}}" class="img-responsive">
    </center>
    <center style="margin-top: 30px;">
        <a class="prelogin_footer_link" href="https://otcopenhouse.com/terms-conditions.html">Terms and Conditions</a>
        &nbsp&nbsp&nbsp&nbsp | &nbsp&nbsp&nbsp&nbsp
        <a class="prelogin_footer_link" href="https://otcopenhouse.com/privacy-policy.html">Privacy Policy</a>
    </center>
</footer>

<script>
    $(document).ready(function () {
        $('.submit_mail').click(function (e) {
            $('.help-block').addClass('hidden');
            e.preventDefault();
            let canSend = true;
            $('input,textarea').each(function () {
                let self = $(this);
                if(!self.val() || self.val() == '')
                {
                    if (!self.hasClass('submit_mail'))
                    {
                        $('.help-block.'+self.attr('name')).removeClass('hidden');
                        canSend = false;
                    }
                }
            });
            if (canSend)
            {
                let route = '{{route('contactUs.mail')}}';
                $.ajax({
                    url: route,
                    type: 'POST',
                    data: {
                        _token:         '{{csrf_token()}}',
                        name:           $('input[name="uname"]').val(),
                        email:          $('input[name="email"]').val(),
                        subject:        $('input[name="subject"]').val(),
                        messageText:    $('textarea[name="messageText"]').val(),
                    },
                    success: function (r) {
                        console.log(r);
                        if(r.status == 'success')
                        {
                            Swal2.fire({
                                toast:true,
                                type: 'success',
                                title: 'Email sent, thank you!',
                                position: 'top-start',
                                timer: 3000,
                                showConfirmButton: false,
                            });
                        }
                        else
                        {
                            Swal2.fire({
                                type: 'error',
                                text: 'An unknown error has occurred.',
                            });
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    },
                });
            }
        });
    });
</script>
</body>
</html>