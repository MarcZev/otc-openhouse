@extends('1a.layouts.prelogin')
@section('content')
<body id="Open_House_Sign_In" class="open-house">
    <!-- Banner -->
    <div class="container-fluid banner">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{route('logout.user', ['url' => \App\Library\Utilities\_Crypt::base64url_encode(route('get.page', ['name' => 'openHouses']))])}}"><button class="btn" id="close">X</button></a>
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <!-- Body Content -->
                <div class="col-sm-12 content p-5 my-4">
                    <center>
                        <h1>{{$data['address']}}</h1>
                        <p class="pb-4 pt-2" id="p1">Agent: {{$data['agent_name']}}</p>

                        <form action="{{route('lead.signIn')}}" method="POST">
                            <input type="hidden" value="{{csrf_token()}}" name="_token">
                            <input type="hidden" value="{{$data['property_id']}}" name="property_id">
                            <input type="hidden" value="{{$data['id']}}" name="open_house_id">
                            @if($errors->has('property_id'))
                                <span class="help-block">{{$errors->first('property_id')}}</span>
                            @endif
                            @if($errors->has('open_house_id'))
                                <span class="help-block">{{$errors->first('open_house_id')}}</span>
                            @endif
                            <div class="form-group row">

                                <div class="col-sm-6">
                                    <div class="col-sm-12 mb-3">
                                        <label for="first_name" class="float-left mb-1">First Name</label>
                                        <input name="first_name" value="{{old('first_name') ?? NULL}}" type="text"
                                               class="form-control" id="fname" required>
                                        @if($errors->has('first_name'))
                                            <span class="help-block">{{$errors->first('first_name')}}</span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 mb-3">
                                        <label for="last_name" class="float-left mb-1">Last Name</label>
                                        <input name="last_name" value="{{old('last_name') ?? NULL}}" type="text"
                                               class="form-control" id="lname" required>
                                        @if($errors->has('last_name'))
                                            <span class="help-block">{{$errors->first('last_name')}}</span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 mb-3">
                                        <label for="phone" class="float-left mb-1">Phone Number</label>
                                        <input maxlength="14" value="{{old('phone') ?? NULL}}" type="text" name="phone"
                                               class="form-control" id="phone" required>
                                        @if($errors->has('phone'))
                                            <span class="help-block">{{$errors->first('phone')}}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <label for="email" class="float-left mb-1">Email</label>
                                        <input value="{{old('email') ?? NULL}}" name="email" type="email"
                                               class="form-control" id="email" required>
                                        @if($errors->has('email'))
                                            <span class="help-block">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-12 mb-3">
                                        <?php
                                            /*
                                             * The reason for this is that the app counterpart saves the text as both
                                             * the display and value in the DB and in the way it references it in the
                                             * app. In order to maintain as max functionality across both platforms
                                             * we will not adjust these values until we can change the DB.
                                             */
                                            $options = [
                                                'ASAP'                      => 'ASAP',
                                                'In the next few months'    => 'In the next few months',
                                                'In the next year'          => 'In the next year',
                                                'I\'m just browsing'        => 'I\'m just browsing',
                                            ];
                                        ?>
                                        <label for="fname" class="float-left mb-1">How Soon Are You Looking to Buy a Home?</label>
                                        <select class="form-control pr-5" name="looking_to_buy">
                                            @foreach($options as $value => $display)
                                                @if($value == old('looking_to_buy'))
                                                    <option value="{{$value}}" selected>{{$display}}</option>
                                                @else
                                                    <option value="{{$value}}">{{$display}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('looking_to_buy'))
                                            <span class="help-block">{{$errors->first('looking_to_buy')}}</span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 mb-3">
                                        <label for="lname" class="float-left mb-1">Are You Currently Working With an Agent?</label>
                                        <select class="form-control" name="working_with_agent">
                                            @if(old('working_with_agent') == 'yes')
                                                <option value="yes" selected>Yes</option>
                                                <option value="no">No</option>
                                            @elseif(old('working_with_agent') == 'no')
                                                <option value="yes">Yes</option>
                                                <option value="no" selected>No</option>
                                            @else
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            @endif
                                        </select>
                                        @if($errors->has('working_with_agent'))
                                            <span class="help-block">{{$errors->first('working_with_agent')}}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <label for="lname" class="float-left mb-1">Are You Pre-Approved For a Mortgage?</label>
                                        <select class="form-control" name="pre_approved_mortgage">
                                            @if(old('pre_approved_mortgage') == 'yes')
                                                <option value="yes" selected>Yes</option>
                                                <option value="no">No</option>
                                            @elseif(old('pre_approved_mortgage') == 'no')
                                                <option value="yes">Yes</option>
                                                <option value="no" selected>No</option>
                                            @else
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            @endif
                                        </select>
                                        @if($errors->has('pre_approved_mortgage'))
                                            <span class="help-block">{{$errors->first('pre_approved_mortgage')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $data['id']])}}"><button type="button" class="btn btn-outline-danger mr-2 mt-4" id="btn-back">Back</button></a>
                            <button class="btn btn-danger mt-4" id="btn-save">Sign In</button>

                        </form>
                    </center>
                </div>
            </div>
            <p id="rights">&COPY; OTC Open House 2019</p>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('input[name="phone"]').mask('(###)-###-####');
        });
    </script>
</body>
@endsection