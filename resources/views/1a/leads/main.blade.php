@extends('1a.layouts.master')
@section('content')
<body id="view-leads" class="main-layout">
    <div class="container-fluid content p-3 mt-2" style="min-height: 110px;">
        <div class="row py-2 content-body px-0 mb-0">
            <div class="col-sm-12">
                <h2 class="text-center py-3">Leads</h2>

                <div class="row px-5 mb-2" id="cards">
                    @foreach($leads as $lead)
                        @if($loop->index < 6) <div class="lead-card col-sm-6 mb-2">
                        @else <div class="lead-card col-sm-6 mb-2 hidden">
                        @endif
                            <div class="card">

                                <div class="card-body pt-3 pb-2 pl-3">
                                    <h4 class="card-title" align="center">{{$lead['name']}}</h4>
                                    <p class="card-text mb-2">
                                        <a href="#"><img src="{{asset('images/icon-address-1.png')}}"> {{$lead['property']['address']}}</a>
                                    </p>
                                    <hr class="mt-0 mb-1">
                                    <p class="card-text mb-0">
                                        <a href="#"><img src="{{asset('images/icon-email-1.png')}}" style="height: 12px;"> {{$lead['email']}}</a>
                                    </p>
                                </div>
                                <div class="card-footer pl-3">
                                    <p class="card-text">
                                        <?php
                                            $in = (string)$lead['phone'];
                                            $output = '('.substr($in,0,3).") ".substr($in,3,3)."-".substr($in,6,4);
                                        ?>
                                        <a href="{{route('get.page.details', ['name' => 'leads', 'id' => $lead['id']])}}" class="stretched-link"><img src="{{asset('images/icon-phone-1.png')}}"> {{$output}}</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <hr class="mb-2" style="border:1px solid #D93149;">

                <center><a id="more" class="view-more">VIEW MORE LEADS</a></center>

            </div>

        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            let moreElements = new showMoreElements('lead-card',6);
            $('#more').click(function () {
                moreElements.viewMore(6);
            });
        });
    </script>
</body>
@endsection