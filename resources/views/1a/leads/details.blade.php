@extends('1a.layouts.master')
@section('content')
<body id="leads-details" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-4 content-body">

            <div class="col-sm-12">
                <form action="{{route('lead.update')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{$data['id']}}">
                    <div class="form-group-row" style="margin: 0 auto;">
                        <div class="col-sm-12 p-0">
                            <input type="text" name="NameFull" disabled value="{{$data['name'] ?? NULL}}" class="form-control" id="name">
                        </div>
                    </div>
                    <div class="form-group-row">
                        <div class="text-center col-sm-12">
                            <button class="btn" id="editMode" type="button">
                                <img id="modeImg" src="{{asset('images/edit.png')}}" ><br><span id="mode">Edit</span>
                            </button>
                            <button class="btn hidden" id="save" type="submit">
                                <img id="modeImg" src="{{asset('images/save.png')}}" ><br><span id="mode">Save</span>
                            </button>
                            <button class="btn hidden" id="cancel" type="button">
                                <img src="{{asset('images/cancel.png')}}" ><br><span>Cancel</span>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row px-3 mt-4">

                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <label for="property" class="float-left mb-1">Property Visited</label>
                                <input name="PropertyVisited" type="text" class="form-control" id="property" disabled required value="{{$data['property']['address'] ?? NULL}}">
                            </div>

                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Phone Number</label>
                                <input maxlength="14" name="phone" type="text" class="form-control" id="lname" disabled required value="{{$data['phone'] ?? NULL}}">
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Email</label>
                                <input name="email" type="email" class="form-control" id="email" disabled required value="{{$data['email'] ?? NULL}}">
                            </div>
                        </div>

                        <div class="col-sm-6 px-4">
                            <div class="col-sm-12 mb-3">
                                <?php
                                    $options = [
                                        'ASAP'                      => 'ASAP',
                                        'In the next few months'    => 'In the next few months',
                                        'In the next year'          => 'In the next year',
                                        'I\'m just browsing'        => 'I\'m just browsing',
                                    ];
                                ?>
                                <label for="fname" class="float-left mb-1">How Soon Are You Looking to Buy a Home?</label>
                                <select class="form-control pr-5" disabled name="looking_to_buy">
                                    @foreach($options as $value => $display)
                                        @if($value == $data['looking_to_buy'])
                                            <option value="{{$value}}" selected>{{$display}}</option>
                                        @else
                                            <option value="{{$value}}">{{$display}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Are You Currently Working With an Agent?</label>
                                <select class="form-control" name="working_with_agent" disabled>
                                    @if($data['working_with_agent'] == 'Yes')
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected>No</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-sm-12 mb-3">
                                <label for="lname" class="float-left mb-1">Are You Pre-Approved For a Mortgage?</label>
                                <select class="form-control" name="pre_approved_mortgage" disabled>
                                    @if($data['pre_approved_mortgage'] == 'Yes')
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected>No</option>
                                    @endif
                                </select>

                            </div>
                        </div>

                    </div>

                    <div class="form-group-row px-3">
                        <div class="col-sm-12">
                            <a href="tel:+1{{$data['phone']??NULL}}">
                                <button class="btn leads-details-button" type="button">
                                    <img src="{{asset('images/icon-phone-2.png')}}"><br> Call
                                </button>
                            </a>
                            <a href="mailto:{{$data['email']??NULL}}">
                                <button class="btn leads-details-button" type="button">
                                    <img src="{{asset('images/icon-email-2.png')}}"><br> Send An Email
                                </button>
                            </a>
                            <a href="sms:1{{$data['phone']??NULL}}">
                                <button class="btn leads-details-button" type="button">
                                    <img src="{{asset('images/icon-text-msg.png')}}"><br> Send a Text Message
                                </button>
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            let editImg = '{{asset('images/edit.png')}}';
            let saveImg = '{{asset('images/save.png')}}';

            $('input[name="phone"]').mask('(###)-###-####');

            $('#editMode').click(function () {
                let self = $(this);
                $('#save').show();
                self.hide();
                self.siblings('#cancel').show();
                $('input, select').each(function () {
                    if(this.name != 'NameFull' && this.name != 'PropertyVisited')$(this).attr('disabled',false);
                });
            });
            $('#save').click(function () {
                //todo save the form
            });
            $('#cancel').click(function () {
                location.reload();
            });
        });
    </script>
</body>
@endsection