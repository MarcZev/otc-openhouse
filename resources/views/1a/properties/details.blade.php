@extends('1a.layouts.master')
@section('content')
<body id="property-details" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row p-0 content-body">
            <div class="col-sm-6 px-0" id="carousel">

                <div id="img-slider" class="carousel slide mx-0" data-ride="carousel" data-interval="false" data-keyboard="true" data-pause="false">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#img-slider" data-slide-to="0" class="active"></li>
                        <!-- <li data-target="#img-slider" data-slide-to="1"></li>
                        <li data-target="#img-slider" data-slide-to="2"></li>
                        <li data-target="#img-slider" data-slide-to="3"></li> -->
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <button class="btn btn-outline-danger hidden" id="upload" style="position: absolute; top: 50%; left: 30%; background-color: #dc3545; color: white;">Upload New Photo</button>
                        <?php
                            if ($data['photo'])
                            {
                                if (strpos($data['photo'], 'cdn.filestackcontent.com') == FALSE)
                                    $data['photo'] = \Illuminate\Support\Facades\Config::get('otc.XCHOP.propertyPath') . $data['photo'];
                            }
                            ?>
                            @if($data['photo'])
                                <img src="{{$data['photo']}}" class="img-responsive image_upload" style="width: 100%;">
                            @else
                                <img src="{{asset('images/map_cropped.jpg')}}" class="img-responsive image_upload" style="width: 100%;">
                            @endif
                        </div>
                        <!--
                        <div class="carousel-item">
                            <img src="images/slide-2.jpg" class="img-responsive" style="height: 500px;">
                        </div>
                        <div class="carousel-item">
                            <img src="images/slide-3.jpg" class="img-responsive" style="height: 500px;">
                        </div>
                        <div class="carousel-item">
                            <img src="images/slide-4.jpg" class="img-responsive" style="height: 500px;">
                        </div> -->
                    </div>

                    <!-- Left and right controls -->
                    <!--
                    <a class="carousel-control-prev" href="#img-slider" data-slide="prev">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#img-slider" data-slide="next">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                    -->
                </div>
            </div>
            <div class="col-sm-6 details">
                <form action="{{route('property.update')}}" method="POST" style="margin-top: -35px;">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <input type="hidden" value="{{$data['id']}}" name="id">
                    <input type="hidden" value="{{$data['photo'] ?? NULL}}" name="photo">
                    <input type="hidden" value="{{old('processed_photo') ?? NULL}}" name="processed_photo">
                    <center>
                        <input disabled type="text" name="address" value="{{$data['address']}}" id="property-address" class="form-control py-0">
                        @if($errors->has('address'))
                            <span class="help-block">{{$errors->first('address')}}</span>
                        @endif
                        @if($errors->has('id'))
                            <span class="help-block">{{$errors->first('id')}}</span>
                        @endif
                    </center>
                    <div class="form-group-row" style="margin-bottom: 30px;">
                        <div class="text-center col-sm-12">
                            <button class="btn" id="editMode" type="button">
                                <img id="modeImg" src="{{asset('images/edit.png')}}" ><br><span id="mode">Edit</span>
                            </button>
                            <button class="btn hidden" id="save" type="submit">
                                <img id="modeImg" src="{{asset('images/save.png')}}" ><br><span id="mode">Save</span>
                            </button>
                            <button class="btn hidden" id="cancel" type="button">
                                <img src="{{asset('images/cancel.png')}}" ><br><span>Cancel</span>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-l-price.png')}}" class="img-responsive">
                            <label for="lprice">List Price</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="price" id="lprice" class="form-control"
                                   value="${{number_format($data['price'],0)}}" disabled>
                            @if($errors->has('price'))
                                <span class="help-block">{{$errors->first('price')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-p-type.png')}}" class="img-responsive">
                            <label for="ptype">Property Type</label>
                        </div>
                        <?php
                            $propertyTypes = config('constants.propertyTypes');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="type" disabled>
                                @foreach($propertyTypes as $value => $display)
                                    @if($value == $data['type'])
                                        <option value="{{$value}}" selected>{{$display}}</option>
                                    @else
                                        <option value="{{$value}}">{{$display}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('type'))
                                <span class="help-block">{{$errors->first('type')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-sq-foot.png')}}" class="img-responsive">
                            <label for="sqfoot">Square Footage</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="square_footage" id="sqfoot" class="form-control"
                                   value="{{number_format($data['square_footage'])}} sq ft" disabled>
                            @if($errors->has('square_footage'))
                                <span class="help-block">{{$errors->first('square_footage')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-rooms.png')}}" class="img-responsive">
                            <label for="brooms"># of Bedrooms</label>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                $bedroomOptions = config('constants.bedroomOptions');
                            ?>
                            <select class="form-control" name="bedrooms" disabled>
                                @foreach($bedroomOptions as $value => $display)
                                    @if($value)
                                        @if($value == $data['bedrooms'])
                                            <option selected>{{$display}}</option>
                                        @else
                                            <option>{{$display}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('bedrooms'))
                                <span class="help-block">{{$errors->first('bedrooms')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-baths.png')}}" class="img-responsive">
                            <label for="bathrooms"># of Bathrooms</label>
                        </div>
                        <?php
                            $bathroomOptions = config('constants.bathroomOptions');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="bathrooms" disabled>
                                @foreach($bathroomOptions as $value => $display)
                                    @if($value == $data['bathrooms'])
                                        <option selected>{{$display}}</option>
                                    @else
                                        <option>{{$display}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('bathrooms'))
                                <span class="help-block">{{$errors->first('bathrooms')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent/s</label>
                        </div>
                        <div class="col-sm-6">
                            <select class="form-control" name="agent" disabled>
                                @if($data['agent'] == 'That\'s Me')
                                    <option selected>That's Me</option>
                                    <option>Someone Else</option>
                                @else
                                    <option selected>Someone Else</option>
                                    <option>That's Me</option>
                                @endif
                            </select>
                            @if($errors->has('agent'))
                                <span class="help-block">{{$errors->first('agent')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent Name</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="agent_name" id="agent_name" class="form-control"
                                   value="{{$data['agent_name']}}" disabled>
                            @if($errors->has('agent_name'))
                                <span class="help-block">{{$errors->first('agent_name')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mt-4" style="border:none;">
                        @if($errors->has('reports'))
                            <span class="help-block" style="margin-bottom: 10px;">{{$errors->first('reports')}}</span>
                        @endif
                        <div class="col-sm-6 pr-2">
                            <a href="{{route('property.openHouses', ['id' => $data['id']])}}">
                                <button type="button" class="btn btn-danger form-control">View Open Houses</button>
                            </a>
                        </div>
                        <div class="col-sm-6 pl-2">
                            <a href="{{route('openHouses.viewReports',['propertyID' => $data['id']])}}"><button type="button" class="btn btn-outline-danger form-control" id="reports">Reports</button></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {

            let price = $('#lprice');
            if(price.val().indexOf('$$') > -1) price.val(price.val().replace('$$','$'));


            $('#lprice, #sqfoot').keyup(function () {
                let self = $(this);
                if (self.is('#lprice'))
                {
                    self.val('$' + currencyFormat(self.val()));
                    if(self.val().indexOf('$$') > -1) self.val(self.val().replace('$$','$'));
                }
                else
                {
                    $(this).val(currencyFormat($(this).val()));
                }
            });

            $('input[name="phone"]').mask('(###)-###-####');

            $('#editMode').click(function () {
                let self = $(this);
                $('#save').show();
                self.hide();
                self.siblings('#cancel').show();
                $('input,select').each(function () {$(this).attr('disabled',false)});
                $('#upload').removeClass('hidden');
            });
            $('#save').click(function () {
                //form is currently saved via submit button
            });
            $('#cancel').click(function () {
                location.reload();
            });

            $('#upload').click(function () {
                let filestackSignatureRoute = '{{route('filestack.signature')}}';
                $.ajax({
                    url: filestackSignatureRoute,
                    method: 'GET',
                    success: function (r) {
                        var policy      = r.policy;
                        var signature   = r.signature;
                        const security  = {
                            policy: policy,
                            signature: signature,
                        };

                        const clientOptions = {security};
                        const api_key = 'AzZlFYkLVRHKkQDAtOrYpz';
                        const client = filestack.init(api_key, clientOptions);
                        const options = {
                            maxFiles: 1,
                            accept: 'image/*',
                            uploadInBackground: false,
                            fromSources: [
                                'local_file_system',
                                'url',
                                'googledrive',
                                'dropbox',
                                'gmail',
                                'onedrive',
                                'onedriveforbusiness',
                                'clouddrive',
                                'box',
                            ],
                            onUploadDone: (res) => {

                                if(res.filesFailed.length > 0)
                                {
                                    Swal2.fire({
                                        title:   'Upload failed',
                                        text:    'Reference code: file-upload-003',
                                    });
                                }
                                if(res.filesUploaded.length > 0)
                                {
                                    let uploadedFile = res.filesUploaded[0];
                                    let urlParts = uploadedFile.url.split('/');
                                    let handle = urlParts[3];
                                    let processURL =
                                        urlParts[0]+'//'+
                                        urlParts[2]+'/'+
                                        'security=p:'+ policy+
                                        ',s:'+signature+
                                        '/resize=w:575,h:600,fit:crop/'+
                                        handle;
                                    let url = uploadedFile.url + '?policy='+policy+'&signature='+signature;
                                    $('input[name="photo"]').val(url);
                                    let propertyImage = $('.image_upload');
                                    propertyImage.attr('src',processURL);
                                    $('input[name="processed_photo"]').val(processURL);
                                } //end of if uploadedFiles == 1
                            }, //end of onUploadDone
                        }; //end options

                        client.picker(options).open();
                    },
                    error: function () {
                        Swal2.fire({
                            title: 'Error',
                            type: 'error',
                            text: 'An unknown error has occurred.',
                        });
                    },
                });
            });
        });
    </script>
</body>
@endsection