<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOpenHouse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();

        //you can manipulate data before validation here, see AddProperty for an example.

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'                  => 'required|date',
            'start_time'            => 'required',
            'end_time'              => 'required',
            'type'                  => 'required',
            'property_id'           => 'required',
            'fromProperty'          => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'date.required'                 => 'Open House Date is required.',
            'start_time.required'           => 'Start time is required.',
            'end_time.required'             => 'End time is required.',
            'type.required'                 => 'Open House Type is required.',
            'property_id.required'          => 'That property does not exist.',
        ];
    }
}
