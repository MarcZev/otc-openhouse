<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data                   = $this->all();
        $data['price']          = preg_replace('/[^0-9]/i', '', $data['price']);
        $data['square_footage'] = preg_replace('/[^0-9]/i', '', $data['square_footage']);

        if($data['agent'] == 'That\'s Me')
            $data['agent_name'] = auth()->user()->first_name.' '.auth()->user()->last_name;

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => 'required|int',
            'address'           => 'required|string',
            'photo'             => 'nullable',
            'price'             => 'required|numeric',
            'square_footage'    => 'required|numeric',
            'type'              => 'required',
            'bedrooms'          => 'required',
            'bathrooms'         => 'required',
            'agent'             => 'required',
            'agent_name'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id.required'               => 'Property ID is required.',
            'id.int'                    => 'Property ID must be an integer.',
            'address.required'          => 'Address is required.',
            'price.required'            => 'Price is required and must be a number.',
            'price.numeric'             => 'Price must be a number.',
            'square_footage.required'   => 'Square Feet is required and must be a number.',
            'square_footage.numeric'    => 'Square Feet must be a number.',
            'type.required'             => 'Property Type is required.',
            'bedrooms.required'         => 'Number of Bedrooms is required.',
            'bathrooms.required'        => 'Number of Bathrooms is required.',
            'agent.required'            => 'You must select an option for agent.',
            'agent_name.required'       => 'The name of the agent is required.',
        ];
    }
}
