<?php

namespace App\Http\Requests;

use App\Library\otc\AddressVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;

class AddProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data                   = $this->all();
        $squareFeet             = (int)str_replace(',','',$data['square_footage']);
        $data['square_footage'] = $squareFeet ? $squareFeet : '';
        $price                  = str_replace(',','',$data['price']);
        $price                  = (double)str_replace('$','',$price);
        $data['price']          = $price ? $price : '';

        if($data['agent'] == 'That\'s Me')
            $data['agent_name'] = auth()->user()->first_name.' '.auth()->user()->last_name;

        $data['address'] = $data['paddress'];

        if (!$data['city'] || !$data['state'] || !$data['zip'])
        {
            $cityStateZip       = explode(',',$data['paddress2']);
            if(isset($cityStateZip[0])) $data['city']   = $cityStateZip[0];
            if (isset($cityStateZip[1]))
            {
                if (preg_match('/(^|[ ,])[A-Z]{2}($|[ ,])/', $cityStateZip[1], $match))
                {
                    $data['state'] = trim($match[0]);
                }
            }
            if(isset($cityStateZip[1]))
            {
                $stateZip       = explode(' ',$cityStateZip[1]);
                if(is_array($stateZip))     $data['zip']    = end($stateZip);
            }
        }

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'           => 'required|int',
            'address'           => 'required|string',
            'city'              => 'string',
            'state'             => 'string|max:2',
            'zip'               => 'string',
            'photo'             => 'nullable',
            'price'             => 'required|numeric',
            'square_footage'    => 'required|numeric',
            'type'              => 'required',
            'bedrooms'          => 'required',
            'bathrooms'         => 'required',
            'agent'             => 'required',
            'agent_name'        => 'required',
            'client_name'       => 'required|string',
            'status'            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'address.required'          => 'Address is required.',
            'price.required'            => 'Price is required.',
            'price.numeric'             => 'Price must be a number.',
            'square_footage.required'   => 'Square Feet is required.',
            'square_footage.numeric'    => 'Square Feet must be a number.',
            'type.required'             => 'Property Type is required.',
            'bedrooms.required'         => 'Number of Bedrooms is required.',
            'bathrooms.required'        => 'Number of Bathrooms is required.',
            'agent_name.required'       => 'The name of the agent is required.',
            'client_name.required'      => 'Client Name is required.',
        ];
    }
}
