<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/22/2019
 * Time: 11:22 AM
 */

namespace App\Http\Controllers;


use App\Http\Requests\UserUpdate;
use App\Library\Utilities\_Crypt;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class UserController extends Controller
{
    protected $model;
    protected $user;

    function __construct()
    {
        $this->model = new User();
        $this->user  = $this->model::find(\auth()->id());
    }

    public function logout($url = NULL)
    {
        Auth::logout();
        if ($url)
        {
            $url = _Crypt::base64url_decode($url);
            return redirect()->to($url);
        }

        return redirect()->route('login');
    }

    public function getProperties()
    {
        $properties = $this->user->properties()->orderBy('created_at', 'desc')->get();
        foreach ($properties as $key => $property)
        {
            if ($property->photo)
                if (strpos($properties[$key]->photo, 'cdn.filestackcontent.com') === FALSE)
                    $properties[$key]->photo = Config::get('otc.XCHOP.propertyPath') . $property->photo;
        }
        return $properties;
    }

    public static function returnCorrectProfilePhoto()
    {
        $photo = auth()->user()->photo ?? NULL;
        if ($photo)
        {
            if (strpos($photo, 'cdn.filestackcontent.com') === FALSE)
                $photo = Config::get('otc.XCHOP.profilePath') . $photo;
        }
        return $photo;
    }

    public function getLeads()
    {
        $leads = $this->user->leads()->get();
        foreach($leads as $lead)
        {
            $lead = Lead::find($lead->id);
            $lead->property = $lead->property()->get();
        }
        return $leads;
    }

    public function getOpenHouses()
    {
        $openHouses = $this->user->openHouses()
            ->select(
                'open_house.*',
                'property_details.address'
            )
            ->orderBy('open_house.date', 'asc')
            ->get();
        return OpenHouseController::openHousesByTimeInterval($openHouses);
    }

    public function update(UserUpdate $request)
    {
        $validated          = $request->validated();
        $validated['phone'] = preg_replace('/[^0-9]/i', '', $validated['phone']);

        $current_password = Auth::user()->getAuthPassword();
        if (Hash::check($validated['current_password'], $current_password))
        {
            if($validated['password'] && $validated['password_confirmation']) $validated['password'] = Hash::make($validated['password']);
            else unset($validated['password']);
            $user               = User::find(auth()->id());
            $user->update($validated);
            return redirect()->back();
        }
        else return redirect()->back()
            ->withErrors(new MessageBag(['current_password' => 'The password is not correct.']));
    }
}