<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/22/2019
 * Time: 1:25 PM
 */

namespace App\Http\Controllers;


use App\Library\otc\Pdf_Restpack;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Crypt;
use App\Library\Utilities\_LaravelTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    public function getFilestackSignature()
    {
        $date = new \DateTime('today +1000 days');
        $date = strtotime($date->format('Y-m-d H:i:s'));
        $policy = array(
            'call' => array(
                'pick',
                'read',
                'store',
                'convert'
            ),
            'expiry' => $date,
        );
        $jsonPolicy = json_encode($policy);
        $urlSafeBase64EncodedPolicy = _Crypt::base64url_encode($jsonPolicy);
        $signature = hash_hmac('sha256',$urlSafeBase64EncodedPolicy,Config::get('constants.FILESTACK_SECRET'));
        return response()->json([
            'policy' => $urlSafeBase64EncodedPolicy,
            'signature' => $signature,
        ]);
    }

    public function generateReport(Request $request)
    {
        $data = _Convert::toArray(json_decode($request->input('data')));
        $view = _LaravelTools::addVersionToViewName('reports.report');
        $data['pre_approved_mortgage_image'] = \config('constants.FILESTACK_PRE_APPROVED_MORTGAGE_IMAGE');
        $data['handshake_image'] = \config('constants.FILESTACK_HANDSHAKE_IMAGE');
        $data['icon_leads_image'] = \config('constants.FILESTACK_ICON_LEADS_IMAGE');
        $doc = trim(view($view, [
            'data' => $data,
        ]));

/*
        return view($view,[
            'data' => $data,
        ]);
*/

        $reportPath = 'Undefined';
        try
        {
            File::isDirectory(public_path('reports')) or File::makeDirectory(public_path('reports'), 0755, true, true);
            File::isDirectory(public_path('reports/properties')) or File::makeDirectory(public_path('reports/properties'), 0755, true, true);
            $reportPath = 'reports/properties/'.$data['propertyID'];
            File::isDirectory(public_path($reportPath)) or File::makeDirectory(public_path($reportPath), 0755, true, true);
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION' => $e, 'reportPath' => $reportPath, __METHOD__ => __LINE__]);
        }

        $pdf_file       = '/report' . '-' . date('Y-m-d-H-i-s') . '.pdf';
        $output         = public_path($reportPath). $pdf_file;
        $pdf            = new Pdf_Restpack($doc);
        $pdf_path       = $pdf->write($output);
        return redirect()->to(asset($reportPath.$pdf_file));
    }
}