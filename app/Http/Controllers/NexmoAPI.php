<?php


namespace App\Http\Controllers;


use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Nexmo\Client;
use Nexmo\Client\Credentials\Basic;
use Nexmo\Verify\Verification;

class NexmoAPI
{
    protected $key;
    protected $secret;
    protected $client;
    protected $verificationInstance;
    protected $fail = FALSE;
    protected $errors = [];

    public function __construct()
    {
        $this->key      = Config::get('constants.NEXMO_API_KEY');
        $this->secret   = Config::get('constants.NEXMO_API_SECRET');
        $basic          = new Basic($this->key,$this->secret);
        $this->client   = new Client($basic);
    }

    public function didFail()
    {
        return $this->fail;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function skip()
    {
        return response()->json([
            'status' => 'skip',
        ]);
    }

    public function verifyPhone(Request $request)
    {
        $phone = $request->input('phone');
        if($phone == Config::get('constants.NEXMO_SKIP_PASS'))
        {
            return $this->skip();
        }
        try {
            $this->verificationInstance = $this->client->verify()->start([
                'number' => '1' . $phone,
                'brand' => 'OTC Open House',
                'code_length' => '4',
                'pin_expiry' => '300'
            ]);
            if ($this->verificationInstance->getResponseData()['status'] != 0)
            {
                $this->fail = TRUE;
                $this->errors['phone'] = 'An unknown error has occurred';
            }
        } catch (\Exception $e) {
            Log::alert([
                'nexmo exception' => $e->getMessage(),
            ]);
            $this->fail     = TRUE;
            if($e->getMessage() == 'The destination number is not in a supported network')
            {
                $this->errors['phone'] = 'The phone number is not valid.';
            }
            elseif ($e->getMessage() == 'Invalid value for param: number')
            {
                $this->errors['phone'] = 'Please enter a valid phone number.';
            }
            else
            {
                $this->errors['phone'] = 'An unknown error has occurred. Reference: vp-nex-001';
            }
            return response()->json([
                'status' => 'fail',
                'message'  => $this->errors['phone'],
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data'   => $this->verificationInstance->getResponseData(),
        ]);
    }

    public function verify($phone)
    {
        try {
            $this->verificationInstance = $this->client->verify()->start([
                'number' => '1' . $phone,
                'brand' => 'OTC Open House',
                'code_length' => '4',
                'pin_expiry' => '60'
            ]);
            if ($this->verificationInstance->getResponseData()['status'] != 0)
            {
                $this->fail = TRUE;
                $this->errors['phone'] = 'An unknown error has occurred';
            }
        } catch (\Exception $e) {
            Log::alert([
                'nexmo exception' => $e->getMessage(),
            ]);
            $this->fail     = TRUE;
            if($e->getMessage() == 'The destination number is not in a supported network')
            {
                $this->errors['phone'] = 'The phone number is not valid.';
            }
        }

        return $this;
    }

    public function getRequestID()
    {
        if(!$this->fail)
        {
            $requestID = $this->verificationInstance->getRequestId();
            session()->put('request_id', $requestID);
            return $requestID;
        }
        return NULL;
    }

    public function verifyCodeAjax(Request $request)
    {
        $data = $request->all();
        $code = $data['verification_code'];
        $request_id = $data['request_id'] ?? session('request_id') ?? NULL;
        Log::alert([
           'nexmo code' => $code,
           'nexmo request_id' => $request_id,
           __METHOD__ => __LINE__,
        ]);
        try{
            $verification = new Verification($request_id);
            $result = $this->client->verify()->check($verification, $code);
        }catch (\Exception $e){

            if ($e->getMessage() == 'The code provided does not match the expected value')
            {
                return response()->json([
                    'status'  => 'fail',
                    'message' => $e->getMessage(),
                ]);
            }
            else
            {
                $result = $this->client->verify()->cancel($request_id);
                Log::alert(['nexmo code verification failure, cancellation data:' => $result->getResponseData()]);
            }
            return response()->json([
                'status'                => 'fail',
                'message'               => $e->getMessage(),
                'requestCancelledData'  => $result->getResponseData(),
            ]);
        }
        return response()->json([
            'status' => 'success',
            'data'   => $result,
        ]);
    }

    public function verifyCode(Request $request)
    {
        $data = $request->all();
        $code = $data['verification_code'];
        $request_id = $data['request_id'] ?? session('request_id') ?? NULL;

        try{
            $verification = new Verification($request_id);
            $result = $this->client->verify()->check($verification, $code);
        }catch (\Exception $e){
            if ($e->getMessage() == 'The code provided does not match the expected value')
            {
                return redirect()->route('register')->withErrors(new MessageBag([
                    'phone' => 'The verification code did not match',
                ]))->withInput([$data['userData']]);
            }
        }
    }

    public function cancelVerification(Request $request)
    {
        $requestID = $request->input('request_id');
        Log::alert([
           'nexmo cancel request_id' => $requestID,
        ]);
        try {
            $result = $this->client->verify()->cancel($requestID);
        }
        catch(\Exception $e) {
            Log::alert([
               'nexmo cancel request exception' => $e->getMessage(),
               __METHOD__ => __LINE__,
            ]);
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage(),
            ]);
        }
        return response()->json([
           'status' => 'success',
           'data' => $result->getResponseData(),
        ]);
    }

    public function resendVerification(Request $request)
    {
        $requestID  = $request->input('request_id');
        $phone      = $request->input('phone');
        Log::alert([
            'nexmo resend request_id' => $requestID,
            'nexmo resend phone'     => $phone,
        ]);
        try {
            $result = $this->client->verify()->cancel($requestID);
            $result = $result->getResponseData();
            Log::alert(['result' => $result]);
            if($result['status'] == 0)
            {
                $newRequestID = $this->verify($phone)->getRequestID();
                if(!is_null($newRequestID))
                {
                    return response()->json([
                       'status'     => 'success',
                       'request_id' => $newRequestID,
                    ]);
                }
                else
                {
                    return response()->json([
                        'status'        => 'fail',
                        'message'       => 'Resend verification failed.',
                    ]);
                }
            }
        }
        catch(\Exception $e)
        {
            Log::alert([
                'nexmo resend request exception' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage(),
            ]);
        }
    }
}