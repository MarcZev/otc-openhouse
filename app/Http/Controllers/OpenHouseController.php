<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/15/2019
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddOpenHouse;
use App\Library\otc\XCHOP_API;
use App\Library\Utilities\_LaravelTools;
use App\Models\Lead;
use App\Models\OpenHouse;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\MessageBag;

class OpenHouseController extends Controller_Parent
{
    protected $comments;
    protected $checkIns;
    protected $checkOuts;

    function __construct()
    {
        $this->model = new OpenHouse();
        parent::__construct();
    }

    public function add(AddOpenHouse $request)
    {
        $validated = $request->validated();
        $this->model->upsert($validated);
        if ((boolean)$validated['fromProperty']) return redirect()->route('property.openHouses', ['id' => $validated['property_id']]);
        return redirect()->route('get.page', ['name' => 'openHouses']);
    }

    /**
     * Deprecated, use UserController@getOpenHouses() instead.
     * @return array
     */
    public function deprecated_byUser()
    {
        $user = User::find(\auth()->id());
        $openHouses = [];
        if ($user)
        {
            $openHouses = $user->openHouses()
                ->select(
                    'open_house.*',
                    'property_details.address'
                )
                ->orderBy('open_house.date', 'asc')
                ->get();
        }
        return self::openHousesByTimeInterval($openHouses);
    }

    /**
     * $ids is a string of comma delimited openHouse ids
     * @param $ids
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewLeadsForOpenHouses($ids)
    {
        $view = 'leads.forOpenHouses';
        $ids = explode(',',$ids);
        $leads = Lead::whereHas('openHouse',function($query) use ($ids){
            $query->whereIn('id', $ids);
        })->get();
        return view(_LaravelTools::addVersionToViewName($view),[
            'leads' => $leads,
        ]);
    }

    public function viewReport($propertyID, $ids = NULL)
    {
        $property = Property::find($propertyID);
        if($property) $openHouses = $property->openHouses()->get();
        else return redirect()->back()->withErrors(new MessageBag([
            'reports' => 'There are no reports for this property.',
        ]));

        if($ids == NULL)
        {
            $ids = [];
            if(!is_null($property))
            {
                foreach ($openHouses as $openHouse)
                {
                    $ids[] = $openHouse->id;
                }
                if(empty($ids)) return redirect()->back()->withErrors(new MessageBag([
                    'reports' => 'There are no reports for this property.',
                ]));
                else $ids = implode(',',$ids);
            }
        }

        $view = 'openHouses.viewReports';
        $signIn =
        $signOut =
        $preApprovedMortgage =
        $lookingToBuy =
        $workingWithAgent =
        $priceLow =
        $priceHigh =
        $priceRight =
        $weight =
        $rating =
        $denominator = 0;
        $openHouseCount = count(explode(',',$ids));

        /* There is no advantage in using the API here, only disadvantages.
        if (isServerLive())
        {
            $xChopApi = new XCHOP_API();
            $r = $xChopApi->setPayload([
                'property_id' => $propertyID,
                'user_id' => \auth()->id(),
                'open_house_id' => $ids,
            ])->execute('report')->getResponse();
            return view(_LaravelTools::addVersionToViewName($view), [
                'signIn' => $r->Data->signIn,
                'signOut' => $r->Data->signOut,
                'preApprovedMortgage' => $r->Data->preApprovedMortgage,
                'lookingToBuy' => $r->Data->lookingToBuy,
                'workingWithAgent' => $r->Data->workingWithAgent,
                'rating' => $r->Data->rating,
                'ids' => $ids,
                'photo' => $r->Data->property->photo,
                'address' => $r->Data->property->address,
                'propertyID' => $propertyID,
                'openHouseCount' => $openHouseCount,
            ]);
        }
        else
        {

        }
        */

        $leads = Lead::whereHas('openHouse', function ($query) use ($ids){
            $query->whereIn('id', explode(',',$ids));
        })->get();
        $ratings = [];
        $openHouseDates = [];
        $minDate = NULL;
        $maxDate = NULL;
        if ($openHouses->isEmpty()) return redirect()->back()->withErrors(new MessageBag([
            'reports' => 'There are no reports for this property.',
        ]));
        foreach ($openHouses as $openHouse)
        {
            $openHouseDates[] = strtotime($openHouse->date);
        }
        foreach ($leads as $lead)
        {
            $signedIn = $lead['status'] == '1' ? TRUE:FALSE;
            if ($signedIn) $signIn++;
            else
            {
                $signIn++;
                $signOut++;
            }
            if ($lead['price_of_property'] == 'Too Low') $priceLow++;
            elseif($lead['price_of_property'] == 'The Price is Right, Bob') $priceRight++;
            else $priceHigh++;

            if($lead['rating']) $ratings[] = $lead['rating'];
            if(strtolower($lead['looking_to_buy']) == 'asap') $lookingToBuy++;
            if(strtolower($lead['working_with_agent']) == 'yes') $workingWithAgent++;
            if(strtolower($lead['pre_approved_mortgage']) == 'yes') $preApprovedMortgage++;
        }
        if (!empty($openHouseDates))
        {
            $openHouseDates = array_unique($openHouseDates);
            $minDate = min($openHouseDates);
            $maxDate = max($openHouseDates);
        }
        $totalRatings = count($ratings);
        if ($totalRatings) $rating = array_sum($ratings)/count($ratings);
        if ($property->photo)
        {
            if(strpos($property->photo,'cdn.filestackcontent.com') === FALSE)
                $property->photo = Config::get('otc.XCHOP.propertyPath').$property->photo;
        }
        return view(_LaravelTools::addVersionToViewName($view), [
            'ids' => $ids,
            'signIn' => $signIn,
            'signOut' => $signOut,
            'lookingToBuy' => $lookingToBuy,
            'workingWithAgent' => $workingWithAgent,
            'preApprovedMortgage' => $preApprovedMortgage,
            'rating' => $rating,
            'address' => $property->address ?? NULL,
            'photo' => $property->photo ?? NULL,
            'propertyID' => $propertyID,
            'openHouseCount' => $openHouseCount,
            'minOpenHouseDate' => $minDate,
            'maxOpenHouseDate' => $maxDate,
            'priceLow' => $priceLow,
            'priceRight' => $priceRight,
            'priceHigh' => $priceHigh,
        ]);
    }

    public function generateOpenHouseReportSpreadsheetAndEmail($propertyID, $ids)
    {
        if (isServerLive())
        {
            $xChopApi = new XCHOP_API();
            $r = $xChopApi->setPayload([
                'property_id' => $propertyID,
                'user_id' => \auth()->id(),
                'open_house_id' => $ids,
            ])->execute('generate_spreadsheet')->getResponse();
            if ($r->Response->status == 'success') {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            return response()->json([
                'status' => 'fail',
            ]);
        }

        return response()->json([
            'status'    => 'fail',
            'message'   => 'Unable to use API',
        ]);
    }

    /**
     * This function requires $this->record to exist.
     * To be used with method chaining.
     * @return $this
     */
    public function withProperty()
    {
        $property = Property::find($this->record->property_id);
        $propertyFields = [
            'address',
            'agent_name',
            'price',
            'type',
            'square_footage',
            'bedrooms',
            'bathrooms'
        ];
        if($property) foreach ($propertyFields as $field) $this->record->{$field} = $property->{$field};
        else foreach ($propertyFields as $field) $this->record->{$field} = NULL;

        return $this;
    }

    /**
     * This function requires $this->record to exist.
     * To be used with method chaining.
     * @return $this
     */
    public function withSignedInLeads()
    {
        $this->record->leads = $this->record->leads()->where('status', 1)->get();

        return $this;
    }

    /**
     * Controller_Parent Constructor Explanation.
     * ________________________________________________
     * Inside of classes that extend 'Controller_Parent' class, some child class methods may require the $id parameter
     * but appear to not use the $id parameter inside of the method. The way that those classes work is as follows:
     * Laravel automatically instantiates an object of the class when a method is used within that class, inside of
     * Controller_Parent child class constructors, the constructor assigns $this->model to be the model relevant to that
     * class. It also calls Controller_Parent constructor. Since it is not possible to inject the $id parameter directly
     * into Controller_Parent constructor, we look at the route and see if there is an 'id' parameter, if there is we
     * assign $this->id to it and fetch the relevant record so that Controller_Parent child classes can use the data.
     * This is why it is still necessary for methods in child classes that will be working with specific records to
     * have an $id parameter.
     *
     * Children classes of Controller_Parent that do not have any sort of id parameter are either to be used for method
     * chaining or are being used for post requests (adding new data, etc.)
     */

    /**
     * @param $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signIn($id)
    {
        $view = 'prelogin.openHouseSignIn';
        return view(_LaravelTools::addVersionToViewName($view), [
            'data' => $this->withProperty()->record(),
        ]);
    }

    /**
     * @param $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signOut($id)
    {
        $view = 'prelogin.openHouseSignOut';
        return view(_LaravelTools::addVersionToViewName($view), [
            'data' => $this->withProperty()->withSignedInLeads()->record(),
        ]);
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getComments($id = 0)
    {
        $this->comments = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse)
            {
                $this->comments = $openHouse->leads()->select('feedback')->where('feedback','!=',NULL)->get();
            }
        }

        return $this;
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getCheckIns($id = 0)
    {
        $this->checkIns = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse) $this->checkIns = $openHouse->leads()->get();
        }

        return $this;
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getCheckOuts($id = 0)
    {
        $this->checkIns = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse) $this->checkOuts = $openHouse->leads()->where('status', '2')->get();
        }

        return $this;
    }

    /**
     * openHouses is a collection
     * @param $openHouses
     * @return array
     */
    public static function openHousesByTimeInterval($openHouses)
    {
        $oh = [
            'upcoming'  => [],
            'pastDue'   => [],
            'all'       => [],
        ];
        $today = strtotime(date('Y-m-d h:i A'));
        foreach ($openHouses as $openHouse)
        {
            $date = strtotime($openHouse->date.' '.$openHouse->start_time);
            $openHouseRecord = new OpenHouseController();
            $openHouse->comments = $openHouseRecord->getComments($openHouse->id)->comments;
            $openHouse->checkIns = $openHouseRecord->getCheckIns()->checkIns;
            $openHouse->checkOuts= $openHouseRecord->getCheckOuts()->checkOuts;
            if($date < $today) $oh['pastDue'][] = $openHouse;
            else $oh['upcoming'][] = $openHouse;
            $oh['all'][] = $openHouse;
        }
        return $oh;
    }
}