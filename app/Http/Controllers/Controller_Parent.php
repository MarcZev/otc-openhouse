<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/16/2019
 * Time: 3:01 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

class Controller_Parent extends Controller
{
    protected $routeName    = NULL; //the name of the route from ->name('')
    protected $id           = 0;    //It is only necessary to specify id once per object.
    protected $record       = [];   //the record fetched via the id provided
    protected $records      = [];   //not implemented, multiple records via multiple ids
    protected $model        = NULL; //the model, specified in child class constructor.

    /**
     * Controller_Parent Constructor Explanation.
     * ________________________________________________
     * Inside of classes that extend 'Controller_Parent' class, some child class methods may require the $id parameter
     * but appear to not use the $id parameter inside of the method. The way that those classes work is as follows:
     * Laravel automatically instantiates an object of the class when a method is used within that class, inside of
     * Controller_Parent child class constructors, the constructor assigns $this->model to be the model relevant to that
     * class. It also calls Controller_Parent constructor. Since it is not possible to inject the $id parameter directly
     * into Controller_Parent constructor, we look at the route and see if there is an 'id' parameter, if there is we
     * assign $this->id to it and fetch the relevant record so that Controller_Parent child classes can use the data.
     * This is why it is still necessary for methods in child classes that will be working with specific records to
     * have an $id parameter.
     *
     * Children classes of Controller_Parent that do not have any sort of id parameter are either to be used for method
     * chaining or are being used for post requests (adding new data, etc.)
     */
    public function __construct()
    {
        $this->routeName = Route::currentRouteName();
        $this->id = Route::current()->parameter('id') ?? 0;

        if($this->id)
        {
            $this->record = $this->model::find($this->id);
        }
    }

    public function getRouteName()
    {
        return $this->routeName;
    }

    public function details($id = 0)
    {
        if($id) $this->id = $id;
        return $this;
    }

    public function record()
    {
        return $this->record;
    }

    public function all()
    {
        if($this->model) return $this->model::all();
        return NULL;
    }

    /**
     * This uses the class model to upsert the data array.
     * It uses Model_Parent upsert method so it will only work if the model extends Model_Parent
     * @param $data
     */
    public function updateOrInsert($data)
    {
        $this->model->upsert($data);
    }
}