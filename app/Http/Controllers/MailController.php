<?php


namespace App\Http\Controllers;


use App\Library\Utilities\_LaravelTools;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController
{
    public function contactUs(Request $request)
    {
        $data = $request->all();
        $view = _LaravelTools::addVersionToViewName('email.contactUs');
        Log::info([
            'data' => $data,
            'view' => $view,
            __METHOD__ => __LINE__,
        ]);
        try {
            Log::info('in try pre Mail::send()');
            Mail::send($view, $data, function ($m) use ($data){
                $m->from('info@otcopenhouse.com')
                  ->to('jgreen@offertoclose.com')
                  ->subject('Contact Us: '.$data['subject']);
            });
            Log::info('in try post Mail::send()');
        } catch (\Exception $e) {
            Log::error([
               'ContactUs mail error' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
            return response()->json([
                'status' => 'fail',
                'exception' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'status' => 'success',
        ]);
    }
}