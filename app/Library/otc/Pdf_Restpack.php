<?php
namespace App\Library\otc;

use App\Library\Utilities\_Variables;
use Illuminate\Support\Facades\Log;


/**
 * Class Pdf_Restpack
 *
 * Documentation for the restpack Html  to PDF API is found at: https://restpack.io/html2pdf/docs#route
 *
 */
class Pdf_Restpack extends PdfMaker
{
    public $conversionParameters = [];

    public $input;
    public $accessToken;
    public $apiUrl;

    public $parameterDefaults = [
        'url'             => null,
        'html'            => null,   // There are a lot of valid options, see documentation
        'json'            => false,
        'cache_ttl'       => 86400, // Time in seconds for the resulting image to be cached for further requests. (def = 1 day)
        'filename'        => null,  // If specified, ensures that the resulting file is saved (on CDN?) with the given name.
        'pdf_page'        => 'Letter;A0|A1|A2|A3|A4|A5|A6|Legal|Letter|Tabloid|Ledger|Full',
        'pdf_width'       => null,  // Custom pdf page width. Must be used together with pdf_height.
                                    // Prefer pdf_page if you don't
                                    // have an exact page size requirement..
                                    // Unit can be either px for pixels, in for inches.
                                    // ...  Pattern: NUM[px|in]
                                    // ...  Example: 700px
        'pdf_height'      => null,  // Custom pdf page height. See pdf_width.
        'pdf_margins'     => '30px',     // CSS style margin sizes.
                                    // 1px 2px 3px 4px would cause 1px top, 2px right, 3px bottom, 4px left margins.
                                    // A single size can be specified for both margins.
                                    // ...  Default: 0
                                    // ...  Example: 30px 20px
        'pdf_orientation' => 'portrait;portrait|landscape', // Page orientation
                                                            // ...  Default: portrait
                                                            // ...  Pattern: portrait | landscape
        'pdf_header'      => null,   // HTML template for page header.
                                     // It should have a valid markup and can contain elements with
                                     // classes 'pageNumber', 'totalPages', 'url', 'title' or 'date'.
                                     // Header is automatically added to all pages.
                                     // Note that you need to have top margins on your documents in order to have the header show up.
                                     // Please add margins using pdf_margins. Check Headers and Footers section for more details.
                                     // ... Example: Page <span class="pageNumber"></span> of <span class="totalPages"></span>

         'pdf_footer'      => null, // HTML template for page footer. Please check pdf_header information for details.
                                    // Example: Page <span class="pageNumber"></span> of <span class="totalPages"></span>
         'css'             => null, // Additional CSS string to be injected into the page before render.
         'js'              => null, // Additional JS string to be injected into the page before render.
         'delay'           => 500,    // Time in milliseconds to delay capture after page load.
         'user_agent'      => null,
         'accept_language' => null,
         'headers'         => null,
         'emulate_media'   => null,
         'allow_failed'    => null,
         'wait'            => null,         // Wait until window load event fires or network becomes idle before capturing the page.
                                            // By default we wait for the load event but if you are capturing pages with async
                                            // content / lazy loaded images etc, waiting for network might work better.
                                            // Third option is dom, which basically waits for domready event on the main window.
                                            // ... Default: load
                                            // ... Pattern: load | network | dom

        'shutter'         => null,          //  Wait until a DOM element matching the provided css selector becomes present on the page.
                                            // You can control when you want to capture the page using this parameter.
                                            // The process times out after 10 seconds.
                                            // ... Example: h1.someclass
    ];

    public function __construct($input, $parameters=[])
    {
        $this->input = $input;
        $this->accessToken = config('otc.Restpack_Token');
        $this->apiUrl = config('otc.Restpack_URL');

        $this->conversionParameters = $this->assignDefaults($parameters, $this->parameterDefaults);
    }
    /**
     * @param string  $outputFile  The path to where the PDF should be written
     * @param array   $parameters
     *
     * @return bool|string|null On failure, returns FALSE; on success, returns the output file path
     */
    public function write($outputFile=null, $parameters=[])
    {
        $doc = $this->input ?? '<html>Input Not Found.<br/><br/>'. date('l jS \of F Y h:i:s A') .' </html>';
        if (strtolower(substr($doc, -7)) != '</html>' ) $doc .= '</html>';

        $token = $this->accessToken;

        $pi = pathinfo($outputFile);
        if (($pi['extension']??'') != 'pdf') $outputFile = $pi['dirname'] . DIRECTORY_SEPARATOR . $pi['filename'] . '.pdf';
        if (!canCreateFile($outputFile)) return false;

        if (count($parameters) > 0) $parameters = $this->assignDefaults($parameters, $this->conversionParameters);
        else $parameters = $this->conversionParameters;

// ... These are the request parameters. You can check the full list of possible parameters
// ...     at https://restpack.io/html2pdf/docs#route
        $params = array_merge($parameters, ['access_token' => $token, 'html' => $doc ,]);
        $postdata = http_build_query(
            $params
        );

// ... Build a POST request
        $opts = ['http' =>
                     [
                         'method'  => 'POST',
                         'header'  => 'Content-type: application/x-www-form-urlencoded',
                         'content' => $postdata,
                     ],
        ];

        $context = stream_context_create($opts);

        try
        {
            $result = file_get_contents($this->apiUrl, false, $context);
            file_put_contents($outputFile, $result);
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception' => $e->getMessage(), 'output file'=>$outputFile, 'postdata'=>$params], '**');
        }
        return $outputFile;
    }
 }