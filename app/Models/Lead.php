<?php

namespace App\Models;


class Lead extends Model_Parent
{
    //use SoftDeletes;
    public $table = 'open_users';

    public function openHouse()
    {
        return $this->belongsTo(OpenHouse::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

}
